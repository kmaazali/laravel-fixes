<?php
/*
Plugin Name: Custom Payment Gateway

Description: Simple custom payment gateway
Version: 1.0
Author: Maaz
*/

function html_form_code() {
    echo '<form name="theForm" action="https://3dgatewaytest.ambankgroup.com/BPG/admin/payment/process" method="post" >';
    echo '<center> <h3>Pay To Bank</h3>';
    echo '</center>';
    echo '<table border="0" width="70%" align="center" >';
    echo '<tr>';
    echo '<td width="35%">MERCHANT_ACC_NO</td> <td width="3%">:</td>';
    echo '<td width="62%"> <input name="MERCHANT_ACC_NO" type="text" value="00000914051399348210454" size="50"></td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>AMOUNT</td> <td>:</td>';
    echo '<td> <input name="AMOUNT" type="text" value="50.00" size="50" > </td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>TRANSACTION_TYPE</td> <td>:</td>';
    echo '<td> <input name="TRANSACTION_TYPE" type="text" value="2" size="50">';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>MERCHANT_TRANID</td> <td>:</td>';
    echo '<td> <input name="MERCHANT_TRANID" type="text" value="48210454" size="50">';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>RESPONSE_TYPE</td> <td>:</td>';
    echo '<td> <input name="RESPONSE_TYPE" type="text" value="XML" size="50">';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>RETURN_URL</td> <td>:</td>';
    echo '<td> <input name="RETURN_URL" type="text" value="https://yes.edu.my" size="50" ></td>';
    echo '</tr>';
    echo '<tr>';

    echo '<td>TXN_DESC</td> <td>:</td>';
    echo '<td> <input name="TXN_DESC" type="text" value="Order from Merchant Test Store" size="50">';
    echo '</td></tr><tr>';
    echo '<td>CUSTOMER_ID</td> <td>:</td>';
    echo '<td> <input name="CUSTOMER_ID" type="text" value="Cust001" size="50" ></td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>FR_HIGHRISK_EMAIL</td> <td>:</td>';
    echo '<td> <input name="FR_HIGHRISK_EMAIL" type="text" value="abc@mail.com" size="50" >';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>FR_HIGHRISK_COUNTRY</td> <td>:</td>';
    echo '<td> <input name="FR_HIGHRISK_COUNTRY" type="text" value="United States" size="50" >';
    echo '</td></tr><tr><td>FR_BILLING_ADDRESS</td> <td>:</td>';
    echo '<td> <input name="FR_BILLING_ADDRESS" type="text" value="2112, Dome Street" size="50" >';
    echo '</td></tr><tr><td>FR_SHIPPING_ADDRESS</td> <td>:</td>';
    echo '<td> <input name="FR_SHIPPING_ADDRESS" type="text" value="2112, Dome Street" size="50" ></td>';
    echo '</tr><tr>';


    echo '<td>FR_SHIPPING_COST</td> <td>:</td>';
    echo '<td> <input name="FR_SHIPPING_COST" type="text" value="10.50" size="50" ></td>';
    echo '</tr><tr>';
    echo '<td>FR_PURCHASE_HOUR</td> <td>:</td>';
    echo '<td> <input name="FR_PURCHASE_HOUR" type="text" value="1020" size="50" ></td>';
    echo '</tr><tr>';
    echo '<td>FR_CUSTOMER_IP</td> <td>:</td>';
    echo '<td> <input name="FR_CUSTOMER_IP" type="text" value="211.2.53.164" size="50" ></td>';
    echo '</tr><tr>';
    echo '<td>SECURE_SIGNATURE</td> <td>:</td>';
    echo '<td> <input name="SECURE_SIGNATURE" type="text" value="" size="128"></td>';
    echo '</tr><tr>';
    echo '<td align="center" colspan="3"><br> <input type="submit" value="Submit" >';
    echo '</td></tr>';
    echo '</table>';
    echo '</form>';

}

function deliver_mail() {

    // if the submit button is clicked, send the email
    if ( isset( $_POST['cf-submitted'] ) ) {

        // sanitize form values
        $name    = sanitize_text_field( $_POST["cf-name"] );
        $email   = sanitize_email( $_POST["cf-email"] );
        $subject = sanitize_text_field( $_POST["cf-subject"] );
        $message = esc_textarea( $_POST["cf-message"] );

        // get the blog administrator's email address
        $to = get_option( 'admin_email' );

        $headers = "From: $name <$email>" . "\r\n";

        // If email has been process for sending, display a success message
        if ( wp_mail( $to, $subject, $message, $headers ) ) {
            echo '<div>';
            echo '<p>Thanks for contacting me, expect a response soon.</p>';
            echo '</div>';
        } else {
            echo 'An unexpected error occurred';
        }
    }
}

function cf_shortcode() {
    ob_start();
    //deliver_mail();
    html_form_code();

    return ob_get_clean();
}

add_shortcode( 'custom_payment_form', 'cf_shortcode' );

?>