<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Politics',
                'parent_id' => 0,
            ],
            [
                'name' => 'Sports',
                'parent_id' => 0,
            ],
            [
                'name' => 'Entertainment',
                'parent_id' => 0,
            ],
            [
                'name' => 'Technology',
                'parent_id' => 0,
            ],
            [
                'name' => 'Fashion',
                'parent_id' => 0,
            ],
            [
                'name' => 'Local',
                'parent_id' => 1,
            ],
            [
                'name' => 'International',
                'parent_id' => 1,
            ],
        ]);
    }
}
