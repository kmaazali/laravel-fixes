<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                "key" => "min_withdraw",
                "value" => "10"
            ],
            [
                "key" => "currency_symbol",
                "value" => "€"
            ]
        ]);
    }
}
