<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Test',
                'last_name' => 'Admin',
                'email' => 'marco.martinelli1983@gmail.com',
                'username' => 'admin',
                'password' => bcrypt('Ahmad12#$5'),
                'date_of_birth' => '1997-03-06',
                'name_of_presentation' => '-MA',
                'profile_picture' => '',
                'small_presentation' => 'This is my About Me, mentioning that I am a developer',
                'role_id' => 2
            ]
        ]);
    }
}
