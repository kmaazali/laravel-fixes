<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            [
                'title' => 'This is News Title',
                'short_description' => 'Here is a description',
                'content' => '<h1>My Post</h1><p>Write Anything or copy</p>',
                'featured_image' => 'http://www.ghanalive.tv/wp-content/uploads/2017/04/Tech-News.jpg',
                'user_id' => 2,
                'category_id' => 1,
            ]
        ]);
    }
}
