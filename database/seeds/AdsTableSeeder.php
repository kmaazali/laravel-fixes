<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisments')->insert([
            [
                'dimensions' => "970x250"
            ],
            [
                'dimensions' => "336x280"
            ]
        ]);
    }
}
