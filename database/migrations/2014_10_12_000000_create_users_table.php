<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('date_of_birth');
            $table->string('name_of_presentation');
            $table->string('profile_picture');
            $table->string('small_presentation');
            $table->string('paypal_email')->nullable();
            $table->string('bank_iban')->nullable();
            $table->string('bank_account_holder')->nullable();
            $table->integer('role_id')->default(1);
            $table->integer('suspended')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
