@extends('layouts.user.master')

@section('content')

    <div class="row">
        <div class="col-md-6">

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <p class="blabla">@lang('content.login_text')</p><br>
                <input placeholder="@lang('content.email_text')" id="email" type="email" class="{{ $errors->has('email') ? 'form-control is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus/>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
                <input placeholder="@lang('content.password_text')" id="password" type="password" class="{{ $errors->has('password') ? 'form-control is-invalid' : '' }}" name="password" required/>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif

                <button class="btn btn-danger" type="submit">@lang('content.signin_text')</button><br><br>
                <a href="{{ route('password.request') }}">@lang('content.forgot_password_text')</a><br>
                <a href="{{ route('register') }}">@lang('content.register_new_account_text')</a>
            </form>
        </div>
        <div class="col-md-4">
            <div class="banner">
                <h3>Advertising</h3>
            </div>

        </div>


    </div>

    <div class="row">
        <div class="col-md-12">

        </div>
    </div>

@endsection
