@extends('layouts.user.master')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                @csrf
                <p class="blabla">@lang('content.signup_text')</p><br>


                <input placeholder="il tuo nome" id="first_name" type="text" class="{{ $errors->has('first_name') ? 'form-control is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus/>
                @if ($errors->has('first_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif


                <input placeholder="il tuo cognome" id="last_name" type="text" class="{{ $errors->has('last_name') ? 'form-control is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required/>
                @if ($errors->has('last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif


                <input placeholder="una tua email valida" id="email" type="email" class="{{ $errors->has('email') ? 'form-control is-invalid' : '' }}" name="email" value="{{ old('email') }}" required/>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif


                <input placeholder="come verrai identificato nel sito" id="username" type="text" class="{{ $errors->has('username') ? 'form-control is-invalid' : '' }}" name="username" value="{{ old('username') }}" required/>
                @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif


                <input placeholder="la password per accedere al sito" id="password" type="password" class="{{ $errors->has('password') ? 'form-control is-invalid' : '' }}" name="password" required/>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif


                <input placeholder="@lang('content.confirm_pass_text')" id="password-confirm" type="password" name="password_confirmation" required/>
                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif


                <input placeholder="la tua data di nascita" id="date_of_birth" type="date" class="{{ $errors->has('date_of_birth') ? 'form-control is-invalid' : '' }}" name="date_of_birth" value="{{ old('date_of_birth') }}" required/>
                @if ($errors->has('date_of_birth'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                    </span>
                @endif


                <input placeholder="nome con cui firmerai gli articoli" id="name_of_presentation" type="text" class="{{ $errors->has('name_of_presentation') ? 'form-control is-invalid' : '' }}" name="name_of_presentation" value="{{ old('name_of_presentation') }}" required/>
                @if ($errors->has('name_of_presentation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name_of_presentation') }}</strong>
                    </span>
                @endif


                <input placeholder="immagine del tuo profilo" type="file" id="profile_picture" class="{{ $errors->has('profile_picture') ? 'form-control is-invalid' : '' }}" name="profile_picture" value="{{ old('profile_picture') }}" required/>
                @if ($errors->has('profile_picture'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('profile_picture') }}</strong>
                    </span>
                @endif



                <textarea placeholder="una piccola presentazione di se stessi" class="form-control" cols="20" rows="5" name="small_presentation" required></textarea><br>
                @if ($errors->has('small_presentation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('small_presentation') }}</strong>
                    </span>
                @endif

                <label style="display: block;padding-left: 15px;text-indent: -15px;">
                    <input style="width: 13px;height: 13px;padding: 0;margin:0;vertical-align: bottom;position: relative;top: -1px;*overflow: hidden;" type="checkbox" name="agree" value="agree" /> Sending <a href="/policies.html">News By Email</a>
                </label>
                <br>


                <label style="display: block;padding-left: 15px;text-indent: -15px;">
                    <input style="width: 13px;height: 13px;padding: 0;margin:0;vertical-align: bottom;position: relative;top: -1px;*overflow: hidden;" type="checkbox" name="agree" value="agree" /> Agree with the <a href="/terms.html">terms and conditions</a>
                </label>
                <br>




                <button class="btn btn-danger" type="submit">@lang('content.signup_text')</button><br><br>
                <a href="{{ route('login') }}">@lang('content.login_text')</a><br>

            </form>
        </div>
        <div class="col-md-4">
            <div class="banner">
                <h3>Advertising</h3>
            </div>

        </div>


    </div>

    <div class="row">
        <div class="col-md-12">

        </div>
    </div>


{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Register') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('register') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="invalid-feedback">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Register') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection
