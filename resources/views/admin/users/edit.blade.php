@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">

                        @forelse ($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @empty

                        @endforelse

                        <form class="form-valide" action="{{ route('users.update', $user->id) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="first_name">First Name <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ $user->first_name }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="last_name">Last Name <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ $user->last_name }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="username">Username <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="username" name="username" disabled placeholder="Userame" value="{{ $user->username }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="email">Email <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $user->email }}">
                                </div>
                            </div>
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-lg-4 col-form-label" for="password">Password <span class="text-danger">*</span></label>--}}
                                {{--<div class="col-lg-6">--}}
                                    {{--<input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-lg-4 col-form-label" for="password-confirmation">Confirm Password <span class="text-danger">*</span></label>--}}
                                {{--<div class="col-lg-6">--}}
                                    {{--<input type="password" class="form-control" id="password-confirmation" name="password_confirmation" placeholder="Confirm Password" >--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="date_of_birth">Date of birth <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Date of Birth"  value="{{ $user->date_of_birth }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="role">Role <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <select name="role">
                                        <option value="1" @if ($user->role_id == 1) selected @endif>User</option>
                                        <option value="2" @if ($user->role_id == 2) selected @endif>Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->

@endsection