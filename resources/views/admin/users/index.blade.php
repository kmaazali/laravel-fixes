@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Users </h4>
                    <a href="{{ route('users.create') }}" class="btn btn-primary float-right">Add User</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Date Of Birth</th>
                                <th>Status</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $key => $user)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->date_of_birth }}</td>
                                    <td>@if ($user->suspended == 0)<span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Suspended</span> @endif </td>
                                    <td @if ($user->role_id == 2) class="color-success" @else class="color-primary" @endif>@if ($user->role_id == 2) Admin @else User @endif</td>
                                    <td>
                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            @if ($user->suspended == 0)
                                                <a href="{{ url('/admin/user/' . $user->id . "/suspend") }}" class="btn btn-outline-danger">Suspend</a>
                                            @else
                                                <a href="{{ url('/admin/user/' . $user->id . "/unsuspend") }}" class="btn btn-outline-success">Unsuspend</a>
                                            @endif
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                            <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            <tr>
                                <td colspan="9">No Data Found!</td>
                            </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->

@endsection