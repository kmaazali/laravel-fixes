@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="{{ url('admin/settings/store') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="min_withdraw">Minimum Withdrawal </label>
                                <div class="col-lg-12">
                                    <input type="number" class="form-control" id="min_withdraw" step="1" name="min_withdraw" placeholder="Minimum Withdrawal Limit" value="{{ $min_withdraw }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="currency">Currency Symbol </label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="currency" name="currency" placeholder="Currency Symbol" value="{{ $currency_symbol }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->

@endsection