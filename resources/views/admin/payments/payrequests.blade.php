@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Payment Requests </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Paypal Email</th>
                                <th>IBAN</th>
                                <th>Bank Account Title</th>
                                <th>Payment Mode</th>
                                <th>Amount</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($withdrawals as $key => $withdrawal)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <th>{{ $withdrawal->user->username }}</th>
                                    <td>{{ $withdrawal->user->paypal_email != "" ? $withdrawal->user->paypal_email : "-NONE-"}}</td>
                                    <td>{{ $withdrawal->user->bank_iban != "" ? $withdrawal->user->bank_iban : "-NONE-" }}</td>
                                    <td>{{ $withdrawal->user->bank_account_holder != "" ? $withdrawal->user->bank_account_holder : "-NONE-" }}</td>
                                    <td class="color- {{ $withdrawal->method == "bank" ? "color-danger" : "color-primary" }}">{{ ucfirst($withdrawal->method) }}</td>
                                    <td>{{ $withdrawal->payment->amount }}</td>
                                    <td>
                                        <a href="{{ url('admin/payrequests/paid/' . $withdrawal->id)  }}" class="btn btn-info"><i class="fa fa-pencil"></i> Mark Paid</a>
                                        {{--<form action="{{ route('categories.destroy', $category->id) }}" method="POST">--}}
                                            {{--{{ csrf_field() }}--}}
                                            {{--<input type="hidden" name="_method" value="DELETE">--}}
                                            {{--<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>--}}
                                            {{--<button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>--}}
                                        {{--</form>--}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">No Data Found!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->

@endsection