@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="{{ url('admin/distributefunds') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="amount">Amount </label>
                                <div class="col-lg-12">
                                    <input type="number" class="form-control" id="amount" step="1" name="amount" placeholder="Amount" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->

@endsection