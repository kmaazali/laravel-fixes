@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="{{ url('admin/ads/store') }}" method="post">
                            {{ csrf_field() }}

                            @foreach($ads as $ad)
                                <div class="form-group row">
                                    {{--<label class="col-lg-4 col-form-label" for="val-name">{{ $ad->dimensions }} </label>--}}
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="val-name-{{ $ad->id }}" name="ad{{ $ad->id }}" placeholder="Enter script for {{ $ad->dimensions }}" value="{{ $ad->script }}">
                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->

@endsection