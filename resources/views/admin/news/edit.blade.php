@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-validation">
                        <form class="form-valide" action="{{ route('news.update', $news->id) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="title">Title <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $news->title }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="short_description">Short Description <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="short_description" name="short_description" placeholder="Short Description" value="{{ $news->short_description }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="category">Category <span class="text-danger">*</span></label>
                                <div class="col-lg-6">
                                    <select name="category">
                                        @forelse($categories as $category)
                                            <option value="{{ $category->id }}" @if ($news->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                        @empty
                                            <option value="0" selected>-- NO CATEGORY AVAILABLE --</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->

@endsection