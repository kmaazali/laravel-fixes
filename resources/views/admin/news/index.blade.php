@extends('layouts.admin.master')

@section('content')

    <!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Users </h4>
                    <a href="http://185.62.137.236/news/create" class="btn btn-primary float-right">Add News</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example23" class="table table-hover ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Short Description</th>
                                <th>Featured Image</th>
                                <th>Username</th>
                                <th>Category</th>
                                <th>User Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Short Description</th>
                                <th>Featured Image</th>
                                <th>Username</th>
                                <th>Category</th>
                                <th>User Status</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @forelse($news as $key => $new)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <td>{{ $new->title }}</td>
                                    <td>{{ $new->short_description }}</td>
                                    <td><a href="{{ $new->featured_image }}"><img src="{{ $new->featured_image }}" height="70" width="70"></a></td>

                                    <td><a href="{{ url('profile', $new->user->username) }}" class="text-danger">{{ $new->user->username }}</a></td>
                                    <td>{{ $new->category->name }}</td>
                                    <td>@if ($new->user->suspended == 0)<span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Suspended</span> @endif </td>
                                    <td>
                                        <form action="{{ route('news.destroy', $new->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/news/' . $new->id) }}" class="btn btn-outline-info" target="_blank">View</a>
                                            <a href="{{ route('news.edit', $new->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                            <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No Data Found!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->

@endsection