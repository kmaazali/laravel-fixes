                </div>
                <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved.</footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="/admin-assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/admin-assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="/admin-assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/admin-assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="/admin-assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="/admin-assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
    <script src="/admin-assets/js/lib/morris-chart/raphael-min.js"></script>
    <script src="/admin-assets/js/lib/morris-chart/morris.js"></script>
    <script src="/admin-assets/js/lib/morris-chart/dashboard1-init.js"></script>


    <script src="/admin-assets/js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="/admin-assets/js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="/admin-assets/js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="/admin-assets/js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="/admin-assets/js/lib/calendar-2/pignose.init.js"></script>

    <script src="/admin-assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="/admin-assets/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <script src="/admin-assets/js/scripts.js"></script>
    <!-- scripit init-->

    <script src="/admin-assets/js/lib/toastr/toastr.min.js"></script>
    <!-- scripit init-->
    {{--<script src="/admin-assets/js/lib/toastr/toastr.init.js"></script>--}}

    <script src="/admin-assets/js/custom.min.js"></script>

    <script src="/admin-assets/js/lib/datatables/datatables.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="/admin-assets/js/lib/datatables/datatables-init.js"></script>

    <script>

        @if(Session::has('success'))
            toastr.success('{{ Session::get("success") }}',{
                "positionClass": "toast-top-full-width",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            })
        @endif

        @if(Session::has('info'))
            toastr.info("{{ Session::get('info') }}",{
                "positionClass": "toast-top-full-width",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            })
        @endif

        @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}",{
                "positionClass": "toast-top-full-width",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            })
        @endif

    </script>

    </body>

</html>