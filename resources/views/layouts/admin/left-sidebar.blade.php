<!-- Left Sidebar  -->
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                {{--<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard <span class="label label-rouded label-primary pull-right">2</span></span></a>--}}
                    {{--<ul aria-expanded="false" class="collapse">--}}
                        {{--<li><a href="index.html">Ecommerce </a></li>--}}
                        {{--<li><a href="index1.html">Analytics </a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li> <a  href="{{url('admin/')}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>

                <li class="nav-label">Members</li>
                <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-envelope"></i><span class="hide-menu">Users</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('users.create') }}">Create User</a></li>
                        <li><a href="{{ route('users.index') }}">Browse Users</a></li>
                    </ul>
                </li>
                <li> <a  href="{{ route('admins.index') }}" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Admins</span></a></li>
                <li class="nav-label">Content</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Categories </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('categories.create') }}">Add Category</a></li>
                        <li><a href="{{ route('categories.index') }}">Browse Categories</a></li>
                    </ul>
                </li>
                <li> <a href="{{ route('news.index') }}" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">News</span></a>
                </li>

                <li class="nav-label">Payments</li>
                <li> <a  href="{{url('admin/distributefunds')}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Distribute Funds </span></a>
                </li>
                <li> <a  href="{{url('admin/payrequests')}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Payment Requests </span></a>
                </li>

                <li class="nav-label">Other</li>
                <li> <a href="{{ url('admin/ads') }}" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Ads</span></a></li>
                <li> <a href="{{ url('admin/settings') }}" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Settings</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  -->