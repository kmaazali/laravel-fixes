<!--left-fixed -navigation-->
<div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
            <div class="row">
                <div class="col-md-12">
                    @if(Auth::user())
                    <div class="text-center"><a href="{{ url('notices') }}">{{ $notice_count }} {{ str_plural('Notifiche', $notice_count) }}</a></div>
                    @endif
                    <div class="well well-sm text-center" id="txt"></div>
                </div>
            </div>
            <ul class="nav" id="side-menu">
                @if(Auth::user())
                <li>
                    <a href={{ url('bookmarks') }}><i class="fa fa-home nav_icon"></i>@lang('content.bookmarks_text')</a>
                </li>
                <li>
                    <a href={{ url('history') }}><i class="fa fa-home nav_icon"></i>@lang('content.read_history_text')</a>
                </li>
                <li>
                    <a href="{{ url('myreads') }}"><i class="fa fa-home nav_icon"></i>@lang('content.read_later_text')</a>
                </li>
                @endif

                <h4>Categorie</h4>
                {{--<li>--}}
                    {{--<a href="#"><i class="fa fa-cogs nav_icon"></i>Politics <span class="fa arrow"></span></a>--}}
                    {{--<ul class="nav nav-second-level collapse">--}}
                        {{--<li>--}}
                            {{--<a href="#">Sub Cat 01</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Sub Cat 01</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <!-- /nav-second-level -->

                @forelse($categories as $category)
                <li>
                    <a href="{{ url('category', $category->name) }}"><i class="fa fa-home nav_icon"></i>{{ $category->name }}</a>
                </li>
                @empty
                    No Categories Found
                @endforelse

                <hr>

                @if(Auth::user())
                <li>
                    <a href="#"><i class="fa fa-home nav_icon"></i>Blogger</a>
                </li>
                    @forelse($bloggers as $blogger)


                        <li>
                            <a href="{{ url('timeline', $blogger->username) }}"><i class="fa fa-home nav_icon"></i>{{$blogger->first_name}} {{ $blogger->last_name }}<span style="color:white;background-color: red" class="badge pull-right">@if($blogger->follower[0]->post_count>0){{$blogger->follower[0]->post_count}}@endif</span></a>
                        </li>
                    @empty
                        No Blogger Found!
                    @endforelse
                @endif

            </ul>
            <!-- //sidebar-collapse -->
        </nav>
    </div>
</div>
<!--left-fixed -navigation-->