<!DOCTYPE HTML>
<html>
<head>


    <title>N-Blogger | @yield('title', 'Welcome')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <script>

        (adsbygoogle = window.adsbygoogle || []).push({

            google_ad_client: "ca-pub-6866764300437332",

            enable_page_level_ads: true

        });

    </script>
    <!-- START CSS -->
    <link href="/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">

    @yield('styles')
<style>
    @media only screen
    and (min-device-width : 375px)
    and (max-device-width : 812px)
    and (-webkit-device-pixel-ratio : 3) { /* STYLES GO HERE */
        div.header-left{
            display: inline-flex;
        }
        div.header-right{
            display: inline-flex;
        }
        div.search-box{
            margin-right: 20px;
        }
        .profile_details ul li{
            position: absolute;
        }
        .user-name{
            display: none;
        }
    }


    @media only screen
    and (min-device-width : 375px)
    and (max-device-width : 667px) { /* STYLES GO HERE */
        div.header-left{
            display: inline-flex;
        }
        div.header-right{
            display: inline-flex;
        }
        div.search-box{
            margin-right: 20px;
        }
        .profile_details ul li{
            position: absolute;
        }
        .user-name{
            display: none;
        }
    }
</style>

    <!-- END CSS -->

    <!-- START SCRIPTS -->
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/modernizr.custom.js"></script>
    <script src="/js/underscore-min.js" type="text/javascript"></script>
    <script src="/js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="/js/metisMenu.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/slick.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script>
        $(document).ready(function() {



            var popDisplayed = $.cookie('popDisplayed');
            if(popDisplayed == '1'){
                return false;
            }else{
                setTimeout( function() {
                    subscriptionPopup();
                },10000);
                $.cookie('popDisplayed', '1', { expires: 7 });
            }
        });


    </script>


    <!-- END SCRIPTS -->
    <style>
	
	.popcheckbox
	{
    margin-bottom: -2%;
    position: relative;
    margin-left: -99%;
		
	}
	.checktext{
		
		    margin-left: 2%;
			
    font-weight: bold;
	}
	.poptext{
		    text-align: center;
    font-weight: bold;
		
	}
        ul.pull-right li {
            display: block;
        }
        span.close{
            color:red !important;
        }
        .mpopup {
            display:none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
        }
		.popcheckbox{
			
		}
        .mpopup-content {
           position: relative;
    background-color: white;
    margin: auto;
    padding: 0;
    width: 60%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s;
    border-style: solid;
    border-color: red;
    border-radius: 6px;
        }

        .mpopup-head {
            padding: 2px 16px;
            background-color: white;
            color: black;
        }
        .mpopup-main {padding: 2px 16px;}
        .mpopup-main input[type="text"]{
            width: 30%;
            height: 25px;
            font-size: 15px;
        }
        .mpopup-main input[type="submit"]{
            padding: 5px;
            font-size: 15px;
            font-weight: bold;
            background-color: white;
            outline: none;
            border: none;
            color: #fff;
            cursor: pointer;
        }
        .mpopup-foot {
            padding: 2px 16px;
            background-color: white;
            color: black;
        }

        /* add animation effects */
        @-webkit-keyframes animatetop {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }

        @keyframes animatetop {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }

        /* close button style */
        .close {
                opacity: inherit;
    
    float: right;
    font-size: 28px;
    font-weight: bold;
    
    border-style: solid;
    border-color: red;
    border-radius: 51%;
        }
        .close:hover, .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
</head>



<body class="cbp-spmenu-push" onload="startTime();showpopup();">

<div class="main-content">
    @include('layouts.user.left-sidebar')
    <!-- header-starts -->
    <div class="sticky-header header-section ">
        <div class="header-left">
            <!--toggle button start-->
            <button id="showLeftPush"><i class="fa fa-bars"></i></button>
            <!--toggle button end-->
            <!--logo -->
            <div class="logo">
                <a href="{{ url('/') }}">
                    <img src="/images/logo.png" width="80px" height="30px">
                </a>
            </div>
            <!--//logo-->
            <!--search-box-->
            <div class="search-box">
                <form class="input" method="post" action="/search-news">
                    <input class="sb-search-input input__field--madoka" placeholder="@lang('content.search_placeholder')" type="search" id="input-31" name="term"/>
                    <label class="input__label" for="input-31">
                        <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                        </svg>
                    </label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="display: none" />
                </form>
            </div><!--//end-search-box-->
            <div class="clearfix"> </div>
        </div>
        <div class="header-right">
            <div class="profile_details_left"><!--notifications of menu start -->
                <ul class="nofitications-dropdown">
                    <li class="dropdown head-dpdn">
                        {{--<select class="form-control">--}}
                            {{--<option disabled selected>@lang('content.select_language_text')</option>--}}
                            {{--<option value="af:af:0:za">Afrikaans - af</option>--}}
                            {{--<option value="ar:ar:1:arab">العربية - ar</option>--}}
                            {{--<option value="ar:ary:1:ma">العربية المغربية - ary</option>--}}
                            {{--<option value="az:az:0:az">Azərbaycan - az</option>--}}
                            {{--<option value="az:azb:1:az">گؤنئی آذربایجان - azb</option>--}}
                            {{--<option value="be:bel:0:by">Беларуская мова - bel</option>--}}
                            {{--<option value="bg:bg_BG:0:bg">български - bg_BG</option>--}}
                            {{--<option value="bn:bn_BD:0:bd">বাংলা - bn_BD</option>--}}
                            {{--<option value="bs:bs_BA:0:ba">Bosanski - bs_BA</option>--}}
                            {{--<option value="ca:ca:0:catalonia">Català - ca</option>--}}
                            {{--<option value="ceb:ceb:0:ph">Cebuano - ceb</option>--}}
                            {{--<option value="cs:cs_CZ:0:cz">Čeština - cs_CZ</option>--}}
                            {{--<option value="cy:cy:0:wales">Cymraeg - cy</option>--}}
                            {{--<option value="da:da_DK:0:dk">Dansk - da_DK</option>--}}
                            {{--<option value="de:de_CH:0:ch">Deutsch - de_CH</option>--}}
                            {{--<option value="de:de_CH_informal:0:ch">Deutsch - de_CH_informal</option>--}}
                            {{--<option value="de:de_DE:0:de">Deutsch - de_DE</option>--}}
                            {{--<option value="de:de_DE_formal:0:de">Deutsch - de_DE_formal</option>--}}
                            {{--<option value="el:el:0:gr">Ελληνικά - el</option>--}}
                            {{--<option value="en:en_AU:0:au">English - en_AU</option>--}}
                            {{--<option value="en:en_CA:0:ca">English - en_CA</option>--}}
                            {{--<option value="en:en_GB:0:gb">English - en_GB</option>--}}
                            {{--<option value="en:en_NZ:0:nz">English - en_NZ</option>--}}
                            {{--<option value="en:en_US:0:us">English - en_US</option>--}}
                            {{--<option value="en:en_ZA:0:za">English - en_ZA</option>--}}
                            {{--<option value="eo:eo:0:esperanto">Esperanto - eo</option>--}}
                            {{--<option value="es:es_AR:0:ar">Español - es_AR</option>--}}
                            {{--<option value="es:es_CL:0:cl">Español - es_CL</option>--}}
                            {{--<option value="es:es_CO:0:co">Español - es_CO</option>--}}
                            {{--<option value="es:es_ES:0:es">Español - es_ES</option>--}}
                            {{--<option value="es:es_GT:0:gt">Español - es_GT</option>--}}
                            {{--<option value="es:es_MX:0:mx">Español - es_MX</option>--}}
                            {{--<option value="es:es_PE:0:pe">Español - es_PE</option>--}}
                            {{--<option value="es:es_VE:0:ve">Español - es_VE</option>--}}
                            {{--<option value="et:et:0:ee">Eesti - et</option>--}}
                            {{--<option value="eu:eu:0:basque">Euskara - eu</option>--}}
                            {{--<option value="fa:fa_AF:1:af">فارسی - fa_AF</option>--}}
                            {{--<option value="fa:fa_IR:1:ir">فارسی - fa_IR</option>--}}
                            {{--<option value="fi:fi:0:fi">Suomi - fi</option>--}}
                            {{--<option value="fo:fo:0:fo">Føroyskt - fo</option>--}}
                            {{--<option value="fr:fr_BE:0:be">Français - fr_BE</option>--}}
                            {{--<option value="fr:fr_CA:0:quebec">Français - fr_CA</option>--}}
                            {{--<option value="fr:fr_FR:0:fr">Français - fr_FR</option>--}}
                            {{--<option value="fy:fy:0:nl">Frysk - fy</option>--}}
                            {{--<option value="gd:gd:0:scotland">Gàidhlig - gd</option>--}}
                            {{--<option value="gl:gl_ES:0:galicia">Galego - gl_ES</option>--}}
                            {{--<option value="haz:haz:1:af">هزاره گی - haz</option>--}}
                            {{--<option value="he:he_IL:1:il">עברית - he_IL</option>--}}
                            {{--<option value="hi:hi_IN:0:in">हिन्दी - hi_IN</option>--}}
                            {{--<option value="hr:hr:0:hr">Hrvatski - hr</option>--}}
                            {{--<option value="hu:hu_HU:0:hu">Magyar - hu_HU</option>--}}
                            {{--<option value="hy:hy:0:am">Հայերեն - hy</option>--}}
                            {{--<option value="id:id_ID:0:id">Bahasa Indonesia - id_ID</option>--}}
                            {{--<option value="is:is_IS:0:is">Íslenska - is_IS</option>--}}
                            {{--<option value="it:it_IT:0:it">Italiano - it_IT</option>--}}
                            {{--<option value="ja:ja:0:jp">日本語 - ja</option>--}}
                            {{--<option value="jv:jv_ID:0:id">Basa Jawa - jv_ID</option>--}}
                            {{--<option value="ka:ka_GE:0:ge">ქართული - ka_GE</option>--}}
                            {{--<option value="kk:kk:0:kz">Қазақ тілі - kk</option>--}}
                            {{--<option value="ko:ko_KR:0:kr">한국어 - ko_KR</option>--}}
                            {{--<option value="ku:ckb:1:kurdistan">کوردی - ckb</option>--}}
                            {{--<option value="lo:lo:0:la">ພາສາລາວ - lo</option>--}}
                            {{--<option value="lt:lt_LT:0:lt">Lietuviškai - lt_LT</option>--}}
                            {{--<option value="lv:lv:0:lv">Latviešu valoda - lv</option>--}}
                            {{--<option value="mk:mk_MK:0:mk">македонски јазик - mk_MK</option>--}}
                            {{--<option value="mn:mn:0:mn">Монгол хэл - mn</option>--}}
                            {{--<option value="mr:mr:0:in">मराठी - mr</option>--}}
                            {{--<option value="ms:ms_MY:0:my">Bahasa Melayu - ms_MY</option>--}}
                            {{--<option value="my:my_MM:0:mm">ဗမာစာ - my_MM</option>--}}
                            {{--<option value="nb:nb_NO:0:no">Norsk Bokmål - nb_NO</option>--}}
                            {{--<option value="ne:ne_NP:0:np">नेपाली - ne_NP</option>--}}
                            {{--<option value="nl:nl_NL:0:nl">Nederlands - nl_NL</option>--}}
                            {{--<option value="nl:nl_NL_formal:0:nl">Nederlands - nl_NL_formal</option>--}}
                            {{--<option value="nn:nn_NO:0:no">Norsk Nynorsk - nn_NO</option>--}}
                            {{--<option value="oc:oci:0:occitania">Occitan - oci</option>--}}
                            {{--<option value="pl:pl_PL:0:pl">Polski - pl_PL</option>--}}
                            {{--<option value="ps:ps:1:af">پښتو - ps</option>--}}
                            {{--<option value="pt:pt_BR:0:br">Português - pt_BR</option>--}}
                            {{--<option value="pt:pt_PT:0:pt">Português - pt_PT</option>--}}
                            {{--<option value="ro:ro_RO:0:ro">Română - ro_RO</option>--}}
                            {{--<option value="ru:ru_RU:0:ru">Русский - ru_RU</option>--}}
                            {{--<option value="si:si_LK:0:lk">සිංහල - si_LK</option>--}}
                            {{--<option value="sk:sk_SK:0:sk">Slovenčina - sk_SK</option>--}}
                            {{--<option value="sl:sl_SI:0:si">Slovenščina - sl_SI</option>--}}
                            {{--<option value="so:so_SO:0:so">Af-Soomaali - so_SO</option>--}}
                            {{--<option value="sq:sq:0:al">Shqip - sq</option>--}}
                            {{--<option value="sr:sr_RS:0:rs">Српски језик - sr_RS</option>--}}
                            {{--<option value="su:su_ID:0:id">Basa Sunda - su_ID</option>--}}
                            {{--<option value="sv:sv_SE:0:se">Svenska - sv_SE</option>--}}
                            {{--<option value="ta:ta_LK:0:lk">தமிழ் - ta_LK</option>--}}
                            {{--<option value="th:th:0:th">ไทย - th</option>--}}
                            {{--<option value="tl:tl:0:ph">Tagalog - tl</option>--}}
                            {{--<option value="tr:tr_TR:0:tr">Türkçe - tr_TR</option>--}}
                            {{--<option value="ug:ug_CN:0:cn">Uyƣurqə - ug_CN</option>--}}
                            {{--<option value="uk:uk:0:ua">Українська - uk</option>--}}
                            {{--<option value="ur:ur:1:pk">اردو - ur</option>--}}
                            {{--<option value="uz:uz_UZ:0:uz">Oʻzbek - uz_UZ</option>--}}
                            {{--<option value="vec:vec:0:veneto">Vèneto - vec</option>--}}
                            {{--<option value="vi:vi:0:vn">Tiếng Việt - vi</option>--}}
                            {{--<option value="zh:zh_CN:0:cn">中文 (中国) - zh_CN</option>--}}
                            {{--<option value="zh:zh_HK:0:hk">中文 (香港) - zh_HK</option>--}}
                            {{--<option value="zh:zh_TW:0:tw">中文 (台灣) - zh_TW</option>--}}
                        {{--</select>--}}


                        <div id="google_translate_element"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'en',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                }, 'google_translate_element');
                            }
                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        <script>
                            $(document).ready(function(){
                                $('#google_translate_element').bind('DOMNodeInserted', function(event) {
                                    $('.goog-te-menu-value span:first').html('Translate');
                                    $('.goog-te-menu-frame.skiptranslate').load(function(){
                                        setTimeout(function(){
                                            $('.goog-te-menu-frame.skiptranslate').contents().find('.goog-te-menu2-item-selected .text').html('Translate');
                                        }, 100);
                                    });
                                });
                            });
                        </script>
                    </li>


                </ul>
                <div class="clearfix"> </div>
            </div>
            <div class="profile_details">
                <ul style="list-style-type: none">
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                @if(Auth::user())
                                <span class="prfil-img"><img src=@if(Auth::user()) @if(Auth::user()->profile_picture != "") "/{{ Auth::user()->profile_picture }}" @else "/images/mGmfu.png" @endif @endif width="60px" height="60px" alt=""></span>
                                @endif
                                <div class="user-name">
                                    @if(Auth::guest())
                                        <p>@lang('content.signup_menu_text')</p>
                                    @else
                                        <p>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                    @endif
                                </div>
                                <i class="fa fa-angle-down lnr"></i>
                                <i class="fa fa-angle-up lnr"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i>{{ __('content.login_text') }}</a></li>
                                <li><a href="{{ route('register') }}"><i class="fa fa-user-plus"></i>{{ __('content.signup_text') }}</a></li>
                            @elseif (Auth::user())
                                @if (Auth::user()->role_id == 2)
                                    <li><a href="{{ url('admin/') }}"><i class="fa fa-sign-in"></i>{{ __('content.admin_dashboard_text') }}</a></li>
                                @endif
                                <li><a class='dropdown-item'  href="{{ url('news/create') }}"><i class="fa fa-user"></i>{{ trans_fb('content.add_news_text', 'Add News') }}</a></li>
                                    <li><a class='dropdown-item' href="{{ url('my_drafts') }}"><i class="fa fa-user"></i>{{ trans_fb('content.open_draft_text', 'My Drafts') }}</a></li>
                                <li><a class='dropdown-item' href="{{ url('profile', Auth::user()->username) }}"><i class="fa fa-user"></i>Profile</a></li>
                                <li> <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i>{{ __('content.logout_text') }}
                                    </a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //header-ends -->

    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">

            @if (Session::has('error'))
                <div class="alert alert-danger">{{Session::get('error')}}</div>
                {{ Session::forget('error') }}
            @endif

            @if (Session::has('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
                {{ Session::forget('success') }}
            @endif

            @if (Session::has('info'))
                <div class="alert alert-info">{{Session::get('info')}}</div>
                {{ Session::forget('info') }}
            @endif

