        </div>
    </div>
</div>
<!--footer-->
<div class="footer">
    <ul class="list-inline">
        <li><a href="{{ url('about-us') }}">@lang('content.who_we_are')</a></li>
        <li><a href="{{ url('legal') }}">@lang('content.legal_notice')</a></li>
        <li><a href="{{ url('information') }}">@lang('content.information')</a></li>
    </ul>
</div>
<!--//footer-->

<!-- Classie -->
<script src="/js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.js"> </script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

        <script src="/js/slick.js"></script>
        <script src="/js/metisMenu.min.js"></script>
        <link href="/css/slick.css" rel="stylesheet">
        <link href="/css/slick-theme.css" rel="stylesheet">
<script src="{{ asset('js/share.js') }}"></script>

<script>
    $(document).ready(function(){
        $('.your-class').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    });

    (function () {
        'use strict';

        var sideTabsPaneHeight = function() {
            var navHeight = $('.nav-tabs.nav-tabs-left').outerHeight() || $('.nav-tabs.nav-tabs-right').outerHeight() || 0;

            var paneHeight = 0;

            $('.tab-content.side-tabs .tab-pane').each(function(idx) {
                paneHeight = $(this).outerHeight() > paneHeight ? $(this).outerHeight() : paneHeight;
            });

            $('.tab-content.side-tabs .tab-pane').each(function(idx) {
                $(this).css('min-height', navHeight + 'px');
            });
        };

        $(function() {
            sideTabsPaneHeight();

            $( window ).resize(function() {
                sideTabsPaneHeight();
            });

            $( '.nav-tabs.nav-tabs-left' ).resize(function() {
                sideTabsPaneHeight();
            });
        });
    }());

    var TabBlock = {
        s: {
            animLen: 200
        },

        init: function() {
            TabBlock.bindUIActions();
            TabBlock.hideInactive();
        },

        bindUIActions: function() {
            $('.tabBlock-tabs').on('click', '.tabBlock-tab', function(){
                TabBlock.switchTab($(this));
            });
        },

        hideInactive: function() {
            var $tabBlocks = $('.tabBlock');

            $tabBlocks.each(function(i) {
                var
                    $tabBlock = $($tabBlocks[i]),
                    $panes = $tabBlock.find('.tabBlock-pane'),
                    $activeTab = $tabBlock.find('.tabBlock-tab.is-active');

                $panes.hide();
                $($panes[$activeTab.index()]).show();
            });
        },

        switchTab: function($tab) {
            var $context = $tab.closest('.tabBlock');

            if (!$tab.hasClass('is-active')) {
                $tab.siblings().removeClass('is-active');
                $tab.addClass('is-active');

                TabBlock.showPane($tab.index(), $context);
            }
        },

        showPane: function(i, $context) {
            var $panes = $context.find('.tabBlock-pane');

            // Normally I'd frown at using jQuery over CSS animations, but we can't transition between unspecified variable heights, right? If you know a better way, I'd love a read it in the comments or on Twitter @johndjameson
            $panes.slideUp(TabBlock.s.animLen);
            $($panes[i]).slideDown(TabBlock.s.animLen);
        }
    };

    $(function() {
        TabBlock.init();
    });
</script>

@yield('scripts')

</body>
</html>