<ul class="nav nav-tabs nav-tabs-left nav-centered" role="tablist">
    <li role="presentation" class="profilenav">
        <a href="{{ url('profile', $user->username) }}" role="tab">
            USER
        </a>
    </li>
    <li role="presentation" class="active profilenav">
        <a href="{{ url('timeline', $user->username) }}" role="tab">
            BLOGGER
        </a>
    </li>
</ul>