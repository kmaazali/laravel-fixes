
<!-- mPopup box -->
<div id="mpopupBox" class="mpopup">
    <!-- mPopup content -->
    <div class="mpopup-content">
        <div class="mpopup-head">
            <span class="close" onclick="closepopup()">×</span>
            
        </div>
        <div class="mpopup-main">
            
        </div>
        <div class="mpopup-foot">
            
            <img src="/images/logo.png" alt="logo" class="poplogog">
			<p class="poptext"> Ciao! Ti piace scrivere? Questo è il sito ideale per te! Mettiamo in contatto lettori e<br> scrittori di blog in un unico sito. Tutto ciò che dovrai
			fare è iscriverti, scrivere gli <br>articoli e diffondere in tuoi articoli dove desideri. Più visualizzazioni avrai e più <br>guadagnerai! Penseremo noi a gestire il sito!<br><br> 
DAI INIZIA SUBITO!</p>

	<form class="popcheckbox">
<input type="checkbox" onclick="startTime()" >
	</form>	
<p class="checktext">Do not show anymore</p>
	

        </div>
    </div>
</div>

<div class="cookie_popup" style="
    z-index: 9999;
    position: fixed;
    bottom: 0;
    color:black;
    width: 100%;
    height: 15%;
    background: white;
    display: none;
">
    <span class="close" style="border-radius: 100% !important;color:#5cb85c !important;border-color:#5cb85c !important;" onclick="closepopup()">×</span>
    <div class="col-md-8">
    <h4 style="text-align: left">La tua privacy è importante per noi</h4><br>
    <p style="text-align: left;">Questo sito o gli strumenti terzi da questo utilizzati, utilizzano cookie necessari al funzionamento ed utili alle finalità illustrate nella cookie policy. Se vuoi saperne di più, consulta la nostra informativa sulla privacy. Cliccando su Continua, chiudendo questo banner, scorrendo questa pagina o proseguendo la navigazione in altra maniera, accetterai i nostri termini e condizioni.</p>
    </div>
    <div class="col-md-4"><button onclick="closepopup();" type="button" class="btn btn-success" style="float:right !important;">Continua</button></div>
</div>

@extends('layouts.user.master')

@section('content')

    @include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="row">
            @if(Auth::user())
                <div class="col-md-12">
                    <h4>Based On Your Interest</h4>

                    <div class="your-class">
                        @forelse($boi_news as $n)
                            <a href="{{ url('news', $n->id) }}">
                                <div><img src="{{ $n->featured_image }}" class="img-responsive">
                                    <h3>{{ $n->user->first_name }} {{ $n->user->last_name }} - {{ $n->title }}</h3>
                                    <p><a href="{{ url('readlater', $n->id) }}">Read Later</a></p>
                                </div>
                            </a>
                        @empty
                            <h5>No News Found matching you interest!</h5>
                        @endforelse
                    </div>
                </div>
            @endif
        </div>
        @foreach($feedcategories as $category)
            <div class="row">
                <div class="col-md-12">
                    <h4>{{ $category->name }}</h4>
                    <div class="your-class">
                        @if ($loop->first)
                            @forelse($news1 as $n)

                                    <div><a href="{{ url('news', $n->id) }}"><img src="{{ $n->featured_image }}" class="img-responsive">
                                        <h3>{{ $n->user->first_name }} {{ $n->user->last_name }} - {{ $n->title }}</h3>
                                        <p><a href="{{ url('readlater', $n->id) }}">Read Later</a></p>
                                        </a></div>

                            @empty
                                <h5>No News Found in this Category!</h5>
                            @endforelse
                        @else
                            @forelse($news2 as $n)

                                    <div><a href="{{ url('news', $n->id) }}"><img src="{{ $n->featured_image }}" class="img-responsive">
                                        <h3>{{ $n->user->first_name }} {{ $n->user->last_name }} - {{ $n->title }}</h3>
                                        <p><a href="{{ url('readlater', $n->id) }}">Read Later</a></p>
                                        </a></div>

                            @empty
                                <h5>No News Found in this Category!</h5>
                            @endforelse
                        @endif
                    </div>
                </div>
            </div>
        @endforeach

    </section>

@endsection
