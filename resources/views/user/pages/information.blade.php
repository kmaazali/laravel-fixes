@extends('layouts.user.master')

@section('content')
    @include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="row">

            <div class="col-md-12">
                <h4>Information</h4>

                <div class="col-md-12">
                    Come raccogliamo e usiamo i tuoi dati<br>
                    Pubblicità<br>
                    Il nostro sito web è finanziato dalla pubblicità online. Questo aiuta l'amministratore del sito a mantenerlo aggiornato e aiuta i Blogger ad inserire nuovi contenuti. Quando visiti il nostro sito Web, il tuo browser invia automaticamente determinate informazioni ai nostri partner pubblicitari. Queste informazioni sono il tuo indirizzo IP e l'URL della pagina che stai visitando. Il sito e i nostri partner possono impostare cookie sul browser o leggere cookie già presenti.<br>
                    Utilizziamo Cookie per salvare la sessione dell'Utente e per svolgere altre attività strettamente necessarie al funzionamento di questo Sito Web, ad esempio in relazione alla distribuzione del traffico.<br>
                    Utilizziamo inoltre Cookie per salvare le preferenze di navigazione ed ottimizzare l'esperienza di navigazione dell'Utente. Fra questi Cookie rientrano, ad esempio, quelli per impostare la lingua e la valuta o per la gestione di statistiche da parte del Titolare del sito.<br>
                    <br>
                    La navigazione sul sito autorizza N-Blogger.com e i nostri Partner pubblicitari ad inserire i Cookie sul tuo computer.<br>
                    <br>
                    Le campagne pubblicitarie sono gestite da Google AdSense.<br>
                    <br>
                    Inoltre autorizzi i nostri social network collegati con il sito ad installare i cookie sul tuo dispositivo.<br>
                    <br>
                    Informiamo che la sola navigazione al sito autorizza l'uso dei cookie.<br>
                    <br>
                    Per maggiori informazioni sui cookie installati dai social network, ti invitiamo ad accedere ad i seguenti link:<br>
                    1)https://www.facebook.com/privacy/explanation<br>
                    2)https://twitter.com/it/privacy<br>
                    3)https://policies.google.com/privacy?hl=policies<br>
                    4)https://www.linkedin.com/legal/privacy-policy<br>
                </div>
            </div>

        </div>


    </section>

@endsection
