@extends('layouts.user.master')

@section('content')
    @include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="row">

            <div class="col-md-12">
                <h4>Legal Notices</h4>

                <div class="col-md-12">
                    Di seguito sono inserite tutte le regole del sito:<br>
                    1) Ogni Blogger, registrato sul sito, è responsabile dei contenuti che pubblica. L'amministratore di N-Blogger.com non si assume nessuna responsabilità sui contenuti pubblicati.<br>
                    2) L'amministratore di N-Blogger.com ha la possibilità di bloccare notizie e blogger che trattano argomenti non etici.<br>
                    3) L'amministratore di N-Blogger.com non accetta argomenti non etici. Non accettiamo che si parli positivamente dei temi, come ad esempio:  Violenza, Pornografia, Pedopornografia, Droghe, Prostituzione e in generale di tutti quegli argomenti non etici.<br>
                    4) Ogni violazione autorizza l'Amministratore del sito a bloccare i contenuti del blogger e il blogger stesso.<br>
                    5) Il sito non accetta la violazione della privacy.<br>
                    6) Il sito non accetta la violazione del copiright.<br>
                    7) Il sito Accetta per intero la legge Italiana ed Europea e ogni sua violazione é responsabilità del blogger.<br>
                    8) Ogni utente ha il diritto di segnalare un articolo ma sarà compito dell'amministratore decidere se intervenire sull'articolo e/o Blogger.<br>
                    9) È il sito stesso ad individuare il numero delle pagine visualizzate dagli utenti e a distribuire i fondi in base al numero delle visualizzazioni.<br>
                    10) É vietato utilizzare qualsiasi strumento per aumentare il numero delle visualizzazioni con artifizi.<br>
                    11) Il sito utilizza Cookie. Navigando si accetta che il sito salvi e usi i cookie sul tuo dispositivo elettronico.<br>
                    12) Il sito aderisce alle campagne pubblicitarie di Google AdSense e prima di distribuire i fondi ai Blogger, si trattiene il 10% come indennizzo per le attività di manutenzione.<br>
                    13) E' vietato per i blogger inserire proprie pubblicità sugli articoli da lui scritti.<br>


                </div>
            </div>

        </div>


    </section>

@endsection
