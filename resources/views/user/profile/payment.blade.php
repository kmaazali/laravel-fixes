@extends('layouts.user.master')

@section('content')

@include('layouts.user.advertisement')

<section id="sec-02">
    <div class="tab-container">
        @include('layouts.user.profile-nav')
        <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
            <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                <div class="col-md-12 col-sm-12">

                    <figure class="tabBlock">
                        <ul class="tabBlock-tabs">
                            <a href="{{ url('timeline', $user->username) }}"><li class="tabBlock-tab">{{ trans_fb('content.news_text', 'News') }}</li></a>
                            @if(Auth::user())
                                @if (Auth::user()->id == $user->id)
                                    <a href="{{ url('notices') }}"><li class="tabBlock-tab">{{ trans_fb('content.notifications_text', 'Notice') }}</li></a>
                                    <a href="{{ url('payments') }}"><li class="tabBlock-tab is-active">{{ trans_fb('content.payments_text', 'Payments') }}</li></a>
                                @endif
                            @endif
                        </ul>
                        <div class="tabBlock-content">
                            <div class="tabBlock-pane">
                                <h3>Money Poured : {{ $currency_symbol }} {{ $amount_withdrawn }}</h3>
                                <h3>Pending: {{ $currency_symbol }} {{ $amount_pending }}</h3>
                                <h3>Money still to be paid : {{ $currency_symbol }} {{ $amount_available }}</h3><br>

                                <h4>Withdrawal Methods</h4>
                                @if ($amount_available < $min_withdraw)
                                    Withdrawal not allowed, current amount is less than minimum withdrawal limit
                                @else
                                    <a href="{{ url('withdraw-paypal/?aa=' . $amount_available . '&mw=' . $min_withdraw . '&pay_id=' . $payment->id) }}" class="btn-lg btn-primary" >Paypal</a>
                                    <a href="{{ url('withdraw-bank/?aa=' . $amount_available . '&mw=' . $min_withdraw . '&pay_id=' . $payment->id) }}" class="btn-lg btn-primary">Bank Transfer</a>
                                @endif

                            </div>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection