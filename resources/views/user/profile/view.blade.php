@extends('layouts.user.master')

@section('content')

@include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="tab-container">
            @include('layouts.user.profile-nav')
            <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
                <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                    <div class="col-md-9 col-sm-9">
                        <h3><b>{{ trans_fb('content.account_information', 'Account Information') }}</b></h3>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <a href="{{ url('profile/' . $user->username . '/presentation') }}">View Presentation</a>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="account-info-item">
                                <span class="account-info-label"><img src=@if($user->profile_picture != "") "/{{ $user->profile_picture }}" @else "/images/mGmfu.png" @endif width="60px" height="60px"></span>
                                <div class="account-info account-user-info">
                                    <div class="account-details">
                                        <span class="account-detail account-display-name"><b>{{ $user->first_name }} {{ $user->last_name }}</b></span>
                                        <span class="account-detail">{{ $user->small_presentation }}</span>
                                        @if(Auth::user())
                                            @if(Auth::user()->id == $user->id)
                                                <span class="account-detail"><a href="{{ url('edit/profile') }}">Advance</a></span>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="account-info-item">
                                <span class="account-info-label">{{ trans_fb('content.first_name_text', 'First Name') }}</span>
                                <div class="account-info account-user-info">
                                    <div class="account-details">
                                        <span class="account-detail account-display-name">{{ $user->first_name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="account-info-item">
                                <span class="account-info-label">{{ trans_fb('content.last_name_text', 'Surname') }}</span>
                                <div class="account-info account-user-info">
                                    <div class="account-details">
                                        <span class="account-detail account-display-name">{{ $user->last_name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="account-info-item">
                                <span class="account-info-label">{{ trans_fb('content.email', 'Email') }}</span>
                                <div class="account-info account-user-info">
                                    <div class="account-details">
                                        <span class="account-detail account-display-name">{{ $user->email }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="account-info-item">
                                <span class="account-info-label">{{ trans_fb('content.name_of_presentation', 'Name of Presentation') }}</span>
                                <div class="account-info account-user-info">
                                    <div class="account-details">
                                        <span class="account-detail account-display-name">{{ $user->name_of_presentation }}</span>
                                    </div>
                                </div>
                            </div>


                            <div class="account-info-item">
                                <span class="account-info-label">Account Type</span>
                                <div class="account-info account-user-info">


                                    <div class="account-details">
                                        <span class="account-detail account-display-name">Standard</span>
                                    </div>

                                </div>


                            </div>



dif



                            @if (Auth::user())
                                @if (Auth::user()->id == $user->id)
                                    <div class="account-info-item">
                                        <span class="account-info-label">{{ trans_fb('content.account_setting_text', 'Account Setting') }}</span>
                                        <div class="account-info account-user-info">
                                            <div class="account-details">
                                                <span class="account-detail account-display-name"><a href="{{ url('edit/profile') }}">View or change your account setting</a></span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif


                        </div>

                    </div>


                </div>

            </div>
        </div>


    </section>


@endsection

<script>
    function confirmDelete() {
        var result = confirm('Are you sure you want to delete?');

        if (result) {
            return true;
        } else {
            return false;
        }
    }
</script>