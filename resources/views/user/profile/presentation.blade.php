@extends('layouts.user.master')

@section('content')

@include('layouts.user.advertisement')

<section id="sec-02">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline">
                <li><img src=@if($user->profile_picture != "") "/{{ $user->profile_picture }}" @else "/images/mGmfu.png" @endif width="80px" height="80px"></li>
                <li><a href="{{ url('profile', $user->username) }}">{{ $user->first_name }} {{ $user->last_name }}</a></li>
                @if (Auth::user())
                    @if (Auth::user()->id != $user->id)
                        @if ($is_follower == 0)
                            <li><a href="{{ url('follow', $user->username) }}" class="btn btn-primary">Follow</a></li>
                        @else
                            <li><a href="{{ url('unfollow', $user->username) }}" class="btn btn-primary">Unfollow</a></li>
                        @endif
                    @endif
                @endif
                <li class="pull-right">{{ $followers->count() }} Followers</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form>
                <label>Presentation</label>
                <p>{{ $user->small_presentation }}</p>
            </form>

        </div>

    </div>
    <hr>
    <div class="row">
        @forelse ($followers as $follower)
            <div class="col-md-3 blog">
                <a href="{{ url('profile', Auth::user()->username) }}"><img src=@if($follower->user->profile_picture != "") "/{{ $follower->user->profile_picture }}" @else "/images/mGmfu.png" @endif class="img-responsive"></a>
                <p>{{$follower->user->first_name}} {{$follower->user->last_name}}</p>
            </div>
        @empty
            <h5>No Followers!</h5>
        @endforelse
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="pagination">
                {{$followers->links()}}
                {{--<a href="#">&laquo;</a>--}}
                {{--<a href="#" class="active">1</a>--}}
                {{--<a href="#">2</a>--}}

                {{--<a href="#">&raquo;</a>--}}
            </div>

        </div>

    </div>


</section>


@endsection