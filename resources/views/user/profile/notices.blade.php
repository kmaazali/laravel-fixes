@extends('layouts.user.master')

@section('content')

    @include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="tab-container">
            @include('layouts.user.profile-nav')
            <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
                <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                    <div class="col-md-12 col-sm-12">

                        <figure class="tabBlock">
                            <ul class="tabBlock-tabs">
                                <a href="{{ url('timeline', $user->username) }}"><li class="tabBlock-tab">{{ trans_fb('content.news_text', 'News') }}</li></a>
                                @if(Auth::user())
                                    @if (Auth::user()->id == $user->id)
                                        <a href="{{ url('notices') }}"><li class="tabBlock-tab  is-active">{{ trans_fb('content.notifications_text', 'Notice') }}</li></a>
                                        <a href="{{ url('payments') }}"><li class="tabBlock-tab">{{ trans_fb('content.payments_text', 'Payments') }}</li></a>
                                    @endif
                                @endif
                            </ul>

                            <div class="tabBlock-content">
                                <div class="tabBlock-pane">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Time</th>
                                                <th>Title News</th>
                                                <th>User</th>
                                                <th>Part of the comment</th>
                                                {{--<th>Details</th>--}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($notices as $notice)
                                                <tr>
                                                    <td><a href="{{ url('news', $notice->news->id) }}">{{ $notice->created_at->diffForHumans() }}</a></td>
                                                    <td><a href="{{ url('news', $notice->news->id) }}">{{ $notice->news->title }}</a></td>
                                                    <td><a href="{{ url('news', $notice->news->id) }}">{{ $notice->user->first_name }} {{ $notice->user->last_name }}</a></td>
                                                    <td><a href="{{ url('news', $notice->news->id) }}">{{ str_limit($notice->content, $limit = 25, $end = "...") }}</a></td>
                                                    {{--<td>123</td>--}}
                                                </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4">No New Notifications</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    {{ $notices->links() }}
                                </div>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection