@extends('layouts.user.master')

@section('content')

@include('layouts.user.advertisement')



<section id="sec-02">
    <div class="tab-container">
        @include('layouts.user.profile-nav')
        <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
            <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                <div class="col-md-12 col-sm-12">
                    <h3><b>Account Information</b></h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        @if (Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif

                        <form method="post" action="{{ url('update/profile') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <label>{{ trans_fb('content.first_name_text', 'First Name') }}</label>
                            <input type="text" class="form-control" name="first_name" value="{{ Auth::user()->first_name }}" required>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.last_name_text', 'Surname') }}</label>
                            <input type="text" class="form-control" name="last_name" value="{{ Auth::user()->last_name }}" required>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.email', 'Email') }}</label>
                            <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.password_text', 'Password') }}</label>
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.confirm_pass_text', 'Confirm Password') }}</label>
                            <input type="password" class="form-control" name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.date_of_birth', 'Date of Birth') }}</label>
                            <input type="date" class="form-control" name="date_of_birth" value="{{ Auth::user()->date_of_birth }}">
                            @if ($errors->has('date_of_birth'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('date_of_birth') }}</strong>
                                </span>
                            @endif

                            {{--<input type="text" class="form-control" name="last_name" value="{{ Auth::user()->last_name }}" required>--}}
                            {{--@if ($errors->has('last_name'))--}}
                                {{--<span class="invalid-feedback">--}}
                                    {{--<strong>{{ $errors->first('last_name') }}</strong>--}}
                                {{--</span>--}}
                            {{--@endif--}}

                            <label>{{ trans_fb('content.name_of_presentation', 'Name of Presentation') }}</label>
                            <input placeholder="@lang('content.name_of_presentation')" id="name_of_presentation" type="text" class="form-control" name="name_of_presentation" value="{{ Auth::user()->name_of_presentation }}" required/>
                            @if ($errors->has('name_of_presentation'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name_of_presentation') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.profile_picture_text', 'Profile Picture') }}</label>
                            <input type="file" class="form-control" name="profile_picture"><br>
                            @if ($errors->has('profile_picture'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('profile_picture') }}</strong>
                                </span>
                            @endif

                            <label>{{ trans_fb('content.small_presentation', 'Small Presentation') }}</label>
                            <textarea placeholder="@lang('content.small_presentation')" class="form-control" cols="20" rows="5" name="small_presentation" required>{{ Auth::user()->small_presentation }}</textarea><br>
                            @if ($errors->has('small_presentation'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('small_presentation') }}</strong>
                                </span>
                            @endif


                            @if (\Session::has('pay_pal'))
                            <label>{{ trans_fb('content.paypal_email', 'Paypal Email') }}</label>
                            <input placeholder="{{ trans_fb('content.paypal_email', 'Paypal Email') }}" id="paypal_email" type="email" class="form-control" name="paypal_email" value="{{ Auth::user()->paypal_email }}" />
                            @if ($errors->has('paypal_email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('paypal_email') }}</strong>
                                </span>
                            @endif
                            @endif



                            @if (\Session::has('pay_bank'))
                            <label>{{ trans_fb('content.bank_iban', 'Bank IBAN') }}</label>
                            <input placeholder="{{ trans_fb('content.bank_iban', 'Bank IBAN') }}" id="bank_iban" type="text" class="form-control" name="bank_iban" value="{{ Auth::user()->bank_iban }}" />
                            @if ($errors->has('bank_iban'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('bank_iban') }}</strong>
                                </span>
                            @endif


                            <label>{{ trans_fb('content.bank_account_name', 'Bank Account Holder Name') }}</label>
                            <input placeholder="{{ trans_fb('content.bank_account_name', 'Bank Account Holder Name') }}" id="bank_account_name" type="text" class="form-control" name="bank_account_name" value="{{ Auth::user()->bank_account_holder }}" />
                            @if ($errors->has('bank_account_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('bank_account_name') }}</strong>
                                </span>
                            @endif
                            @endif

                            <button class="btn btn-danger" type="submit">{{ trans_fb('content.save_changes_text', 'Save Changes') }}</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if (\Session::has('pay_key'))
    <li>{!! \Session::get('pay_key') !!}</li>
@endif
@endsection

