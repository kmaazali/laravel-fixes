@extends('layouts.user.master')

@section('content')

@include('layouts.user.advertisement')

<section id="sec-02">
    <div class="tab-container">
        @include('layouts.user.profile-nav')
        <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
            <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                <div class="col-md-12 col-sm-12">

                    <figure class="tabBlock">
                        <ul class="tabBlock-tabs">
                            <a href="{{ url('timeline', $user->username) }}"><li class="tabBlock-tab is-active">{{ trans_fb('content.news_text', 'News') }}</li></a>
                            @if(Auth::user())
                                @if (Auth::user()->id == $user->id)
                                    <a href="{{ url('notices') }}"><li class="tabBlock-tab">{{ trans_fb('content.notifications_text', 'Notice') }}</li></a>
                                    <a href="{{ url('payments') }}"><li class="tabBlock-tab">{{ trans_fb('content.payments_text', 'Payments') }}</li></a>
                                @endif
                            @endif
                        </ul>
                        <div class="tabBlock-content">
                            <div class="tabBlock-pane">
                                @if (Auth::user())
                                    @if ($user->id == Auth::user()->id)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="list-inline">
                                                    <li><a href="{{ url('news/create') }}" class="btn btn-primary">{{ trans_fb('content.add_news_text', 'Add News') }}</a></li>
                                                    <li class="float-right">Articles: {{ $news->count() }}</li>
                                                    <li class="float-right">Total Views: {{ $total_view_count }}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @forelse ($news as $new)
                                    <div class="row">

                                        <a href="{{ url('news', $new->id) }}">
                                            <div class="col-md-3">
                                                <img src="{{ $new->featured_image }}" width="150px" height="100px">
                                            </div>
                                            <div class="col-md-7" style="word-wrap: break-word;">
                                                <h5><b>{{ $new->title }}</b></h5>
                                                <p>{{ $new->short_description }}</p>
                                                <p><sub>{{ $new->views->views }} Views</sub></p>
                                            </div>
                                        </a>
                                        @if (Auth::user())
                                            @if ($user->id == Auth::user()->id)
                                            <div class="col-md-2">
                                                <ul class="pull-right">
                                                    {{--<li><a href=""><span><i class="fa fa-times" aria-hidden="true"></i></span></a></li>--}}
                                                    {{--<li><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></li>--}}


                                                        <a href="{{ url('news/' . $new->id . '/edit') }}" class=""><i class="fa fa-pencil"></i> Edit</a>
                                                        <a href="{{url("delete/news/".$new->id)}}"><button  style='background:none!important;color:inherit;border:none;padding:0!important;font: inherit;'><i class="fa fa-times" style="width: 0;"></i> Delete</button></a>


                                                </ul>
                                            </div>
                                            @endif
                                        @endif
                                    </div>
                                @empty
                                    <div class="row">
                                        <div class="alert alert-info">{{ trans_fb('content.no_news_text', 'No News to display') }}</div>
                                    </div>
                                @endforelse
                                {{--<div class="row">--}}

                                    {{--<div class="col-md-3">--}}
                                        {{--<img src="images/001.jpeg" width="150px" height="100px">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-7">--}}
                                        {{--<h5><b>Twisted | Episode 1 - 'Like A Circle In A Spiral' | Nia Sharma | A Web Series By Vikram Bhatt</b>--}}

                                        {{--</h5>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<ul class="pull-right">--}}
                                            {{--<li><span><i class="fa fa-times" aria-hidden="true"></i></span></li>--}}
                                            {{--<li><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></li>--}}

                                        {{--</ul>--}}

                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--<div class="row">--}}

                                    {{--<div class="col-md-3">--}}
                                        {{--<img src="images/001.jpeg" width="150px" height="100px">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-7">--}}
                                        {{--<h5><b>Twisted | Episode 1 - 'Like A Circle In A Spiral' | Nia Sharma | A Web Series By Vikram Bhatt</b>--}}

                                        {{--</h5>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<ul class="pull-right">--}}
                                            {{--<li><span><i class="fa fa-times" aria-hidden="true"></i></span></li>--}}
                                            {{--<li><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></li>--}}

                                        {{--</ul>--}}

                                    {{--</div>--}}

                                {{--</div>--}}



                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="pagination">
                                            {{--<a href="#">&laquo;</a>--}}
                                            {{--<a href="#" class="active">1</a>--}}
                                            {{--<a href="#">2</a>--}}

                                            {{--<a href="#">&raquo;</a>--}}
                                            {{ $news->links() }}
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script>
    $( document ).ready(function() {
        if(window.location.href.indexOf("timeline")){
            console.log('done');
            $(".profilenav").removeClass("active");
            $( ".profilenav" ).first().addClass("active");
        }
    });
</script>
