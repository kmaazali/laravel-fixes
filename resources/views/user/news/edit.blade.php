@extends('layouts.user.master')

@section('styles')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/froala_editor.css">
<link rel="stylesheet" href="/css/froala_style.css">
<link rel="stylesheet" href="/css/plugins/code_view.css">
<link rel="stylesheet" href="/css/plugins/colors.css">
<link rel="stylesheet" href="/css/plugins/emoticons.css">
<link rel="stylesheet" href="/css/plugins/image_manager.css">
<link rel="stylesheet" href="/css/plugins/image.css">
<link rel="stylesheet" href="/css/plugins/line_breaker.css">
<link rel="stylesheet" href="/css/plugins/table.css">
<link rel="stylesheet" href="/css/plugins/char_counter.css">
<link rel="stylesheet" href="/css/plugins/video.css">
<link rel="stylesheet" href="/css/plugins/fullscreen.css">
<link rel="stylesheet" href="/css/plugins/file.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

@endsection

@section('content')

@include('layouts.user.advertisement')

<section id="sec-02">
    <div class="tab-container">
        @include('layouts.user.profile-nav')
        <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
            <div class="tab-pane fade in active" id="web-dev" role="tabpanel">
                <div class="col-md-12 col-sm-12">
                    <figure class="tabBlock">
                        <ul class="tabBlock-tabs">
                            <a href="{{ url('timeline', Auth::user()->username) }}"><li class="tabBlock-tab is-active">{{ trans_fb('content.news_text', 'News') }}</li></a>
                            <a href="notice.html"><li class="tabBlock-tab">{{ trans_fb('content.notifications_text', 'Notice') }}</li></a>
                            <a href="withdrawal.html"><li class="tabBlock-tab">{{ trans_fb('content.payments_text', 'Payments') }}</li></a>
                        </ul>
                        <div class="tabBlock-content">
                            <div class="tabBlock-pane">
                                <p><a href="{{ url('news/create') }}">{{ trans_fb('content.add_news_text', 'Add News') }}</a></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="post" action="{{ url('news', $news->id) }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="PATCH">
                                            <input type="text" name="title" placeholder="{{ trans_fb('content.news_title_text', 'Title') }}" class="form-control" value="{{ $news->title }}" required>
                                            <textarea class="form-control" name="short_description" rows="6" placeholder="{{ trans_fb('content.news_short_description_text', 'Short Description') }}" required>{{ $news->short_description }}</textarea>
                                            <br>
                                            <textarea name="news_content" id="newsEditor">{{ $news->content }}</textarea>
                                            <br>
                                            {{--<div id="editor">--}}
                                                {{--<div id='edit' style="margin-top: 30px;">--}}
                                                    {{--<h1>Click Here To Edit News</h1>--}}

                                                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<a href="https://www.froala.com/wysiwyg-editor/docs/options#initOnClick" target="_blank" title="initOnClick option">Click Here</a> when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>--}}

                                                    {{--<p><strong>Click here to initialize the WYSIWYG HTML editor on this text.</strong></p>--}}
                                                    {{--</div>--}}
                                                {{--</div><br>--}}

                                            <select name="category">
                                                <option value="">--SELECT CATEGORY--</option>
                                                @forelse($categories as $category)
                                                    <option value="{{ $category->id }}" @if ($news->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                                @empty

                                                @endforelse
                                            </select>
                                            <br>
                                            <br>

                                            <img src="{{ $news->featured_image }}" width="250">
                                            <input type="file" name="file1" class="form-control">
                                            <input type="file" name="file2" class="form-control">
                                            <input type="text" id="date_of_pub" name="date_of_pub" placeholder="{{ trans_fb('content.news_date_of_pub_text', 'Day & Time of Publication YYYY-MM-DD HH-MM-SS') }}" class="form-control" value="{{ $news->schedule_time }}">
                                            <textarea class="form-control" name="collaborators" rows="5" placeholder="News written in collaboration with the selected users here.">{{ $collaborators }}</textarea><br>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <button type="submit" class="btn btn-danger" name="submit" value="publish">{{ trans_fb('content.news_save_text', 'Publish') }}</button>
                                                </div>
                                                <div class="col-md-offset-2 col-md-5">
                                                    <button type="submit" id="draft-btn" class="btn btn-primary" name="submit" value="draft">{{ trans_fb('content.news_draft_text', 'Save Draft') }}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="/js/froala_editor.min.js" ></script>
<script type="text/javascript" src="/js/plugins/align.min.js"></script>
<script type="text/javascript" src="/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="/js/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="/js/plugins/image.min.js"></script>
<script type="text/javascript" src="/js/plugins/file.min.js"></script>
<script type="text/javascript" src="/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="/js/plugins/link.min.js"></script>
<script type="text/javascript" src="/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="/js/plugins/video.min.js"></script>
<script type="text/javascript" src="/js/plugins/table.min.js"></script>
<script type="text/javascript" src="/js/plugins/url.min.js"></script>
<script type="text/javascript" src="/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="/js/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="/js/plugins/save.min.js"></script>
<script type="text/javascript" src="/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="/js/languages/ro.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


<script>


    $( document ).ready(function() {
        var d = new Date();
        $( "#date_of_pub" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto",
            maxDate:(1)
        });
    });
    $(function(){
        $('#newsEditor').froalaEditor({
            key:'0C4H4D3A11dD5B4B4A4H3I3E3B7A4D4F-11jD4dkgnmnE-11F5mh1A-7==',
            enter: $.FroalaEditor.ENTER_P,
            initOnClick: false,
            placeholderText: 'Write News Content Here',
            imageUploadURL: '/maaz.php'

        })

        $('#newsEditor').froalaEditor('html.set', '{!! $news->content !!}');
    });
</script>

@endsection
