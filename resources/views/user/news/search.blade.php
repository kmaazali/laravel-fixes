@extends('layouts.user.master')

@section('content')

    @include('layouts.user.advertisement')


    <section id="sec-02">
        <div class="row">
            <div class="col-md-12">
                <div class="your-class">
        @forelse ($news as $n)


                            <div><a href="{{ url('news', $n->id) }}"><img src="{{ $n->featured_image }}" class="img-responsive">
                                    <h3>{{ $n->user->first_name }} {{ $n->user->last_name }} - {{ $n->title }}</h3>
                                    <p><a href="{{ url('readlater', $n->id) }}">Read Later</a></p>
                                </a></div>



                {{--@if (Auth::user())--}}
                    {{--@if ($n->user->id == Auth::user()->id)--}}
                        {{--<div class="col-md-2">--}}
                            {{--<ul class="pull-right">--}}
                                {{--<li><a href=""><span><i class="fa fa-times" aria-hidden="true"></i></span></a></li>--}}
                                {{--<li><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></li>--}}

                                {{--<form action="{{ route('news.destroy', $n->id) }}" method="POST">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<input type="hidden" name="_method" value="DELETE">--}}
                                    {{--<a href="{{ url('news/' . $n->id . '/edit') }}" class=""><i class="fa fa-pencil"></i> Edit</a>--}}
                                    {{--<a><button style='background:none!important;color:inherit;border:none;padding:0!important;font: inherit;' onclick="return confirm('Are you sure?')"><i class="fa fa-times" style="width: 0;"></i> Delete</button></a>--}}
                                {{--</form>--}}

                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                {{--@endif--}}
            {{--</div>--}}
        @empty
            <h3>No News Found!</h3>
        @endforelse
                </div>
            </div>

        {{--<div class="row">--}}
            {{--<div class="col-md-12 text-center">--}}
                {{--<div class="pagination">--}}
                    {{--@if (isset($rls))--}}
                        {{--{{ $rls->links() }}--}}
                    {{--@elseif (isset($bms))--}}
                        {{--{{ $bms->links() }}--}}
                    {{--@else--}}
                        {{--{{$news->links("pagination::bootstrap-4")}}--}}
                    {{--@endif--}}
                {{--</div>--}}


            {{--</div>--}}
        {{--</div>--}}
            </div>

    </section>
    <div class="header">Users</div>

    <section id="sec-03">
        <div class="row">
            <div class="col-md-12">
                <div class="your-class">
        @forelse ($user as $use)


                <a href="timeline/{{$use->username}}">

                        <img src="/images/mGmfu.png" width="200px" height="100px">


                        <h5><b>{{ $use->first_name }}</b></h5>
                        <p>{{ $use->last_name }}</p>

                </a>


        @empty
            <h3>No Users Found!</h3>
        @endforelse



                </div>
            </div>
        </div>


    </section>




@endsection