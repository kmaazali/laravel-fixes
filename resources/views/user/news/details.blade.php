@extends('layouts.user.master')
@section('title', $news->title)
@section('content')


@include('layouts.user.advertisement')

<section id="sec-02">
    <div class="row">
        <div class="col-md-7" style="word-break: break-word">
            <h2>{{ $news->title }}</h2>

            <ul class="list-inline">
                <li>{{ Carbon\Carbon::parse($news->schedule_time)->format('d-m-Y H:i') }} -/- {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news->schedule_time)->diffForHumans()}}</li>
                <li><a href="{{ url('profile', $news->user->username) }}">{{ $news->user->first_name }} {{ $news->user->last_name }}</a>@if (Auth::user()) @if($news->user_id == Auth::user()->id) // <a href="/news/{{$news->id}}/edit" class="btn-sm btn-info"> Edit Page </a> @endif @endif </li>
                <li class="float-right"><a href="#" id="read"><img src="{{ url('images/speaker.png') }}" width="40"></a></li>
                <li class="float-right label label-success">{{ $news->views->views }} Views</li>
                <li class="float-right"><a href="{{ url('togglebookmark', $news->id) }}"><i @if($is_bookmarked) class="fa fa-bookmark" @else class="fa fa-bookmark-o" @endif></i> </a></li>
                <li class="float-right"><a href="{{url('readlater',$news->id)}}">Read Later</a></li>
            </ul>
            <img class="img-responsive" src="{{$news->featured_image}}">

            {!! $news->content !!}

            @if($collaborators->count() > 0) <p style="font-style: italic">Written in collaboration with: @foreach($collaborators as $collaborator) {{ $collaborator->username }}@if(!$loop->last), @endif  @endforeach </p>@endif

            <hr style="border: 1px solid;">

            <div class="comments">
                <h1>Comments ({{ count($news->comments) }})</h1>
                <form method="post" action="{{ url('comment/publish', $news->id) }}">
                    {{ csrf_field() }}
                    <textarea required placeholder="Share your thoughts..." class="comment-content" maxlength="200" name="comment_content"></textarea>
                    <div class="insert-text">
                        <span id="chars">200</span> characters remaining
                        <span>
                            <input type="submit" value="Publish" />
                        </span>
                    </div>
                </form>
                <div class="list-comments">
                    @forelse ($news->comments as $comment)
                        <div>
                            <p><span class="username"><a href="{{ url('profile', $comment->user->username) }}">{{ $comment->user->first_name }} {{ $comment->user->last_name }}</a></span> | {{ $comment->created_at->diffForHumans() }}
                                @if(Auth::user())
                                    @if(Auth::user()->id == $news->user->id || Auth::user()->id == $comment->user->id)
                                    | <a href="{{ url('comment/delete', $comment->id) }}">Delete</a></p>
                                    @endif
                                @endif
                            <p>{{ $comment->content }}</p>
                        </div>
                    @empty
                        <div class="row">
                            <div class="col-md-offset-4"> No Comments Found!</div>
                        </div>
                    @endforelse
                </div>
            </div>

        </div>
        <div class="col-md-2">
            {!!
                Share::currentPage($news->title, [], '<ul class="social">', '</ul>')
                ->facebook()
                ->twitter()
                ->googlePlus()
                ->linkedin()
             !!}
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>RECENTLY ADDED POSTS</b></div>
                <div class="panel-body">
                    <ul class="list-inline">
                        @foreach($popular_posts as $pop_post)
                            <li class="clearfix">
                                <a class="ad_img" href="{{$pop_post->id}}">
                                    <img src={{url($pop_post->featured_image)}} width="50px" height="50px">
                                    <span class="ad_heading">{{$pop_post->title}}</span>
                                </a>
                            </li>
                            @endforeach


                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><b>POPULAR POSTS</b></div>
                <div class="panel-body">
                    <ul class="list-inline">
                        @foreach($hits as $hi)
                            <li class="clearfix">
                                <a class="ad_img" href="{{$hi->id}}">
                                    <img src={{url($hi->featured_image)}} width="50px" height="50px">
                                    <span class="ad_heading">{{$hi->title}}</span>
                                </a>
                            </li>
                        @endforeach


                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@section('scripts')

    <script type="text/javascript">
        function decodeHTMLEntities(text) {
            var entities = [
                ['amp', '&'],
                ['apos', '\''],
                ['#x27', '\''],
                ['#x2F', '/'],
                ['#39', '\''],
                ['#47', '/'],
                ['lt', '<'],
                ['gt', '>'],
                ['nbsp', ' '],
                ['quot', '"']
            ];

            for (var i = 0, max = entities.length; i < max; ++i)
                text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

            return text;
        }


        var content = '{{ strip_tags($news->content) }}';
        content = decodeHTMLEntities(content);

        $('#read').on('click', function () {
            var msg = new SpeechSynthesisUtterance(content);
            var voices = window.speechSynthesis.getVoices();
            console.log(voices);
            msg.voice = voices[27];
            msg.volume = 1; // 0 to 1
            msg.rate = 0.6; // 0.1 to 10
            msg.pitch = 1; //0 to 2
            window.speechSynthesis.speak(msg);
        });

        var maxLength = 200;
        var dt = new Date();
        var time = dt.getMonth() + "/" + dt.getDate() + "/" + dt.getFullYear() + " "+ dt.getHours() + ":" + dt.getMinutes();
        $(document).ready(function(){
            $('textarea').keyup(function() {
                var length = $(this).val().length;
                var length = maxLength-length;
                $('#chars').text(length);
            });
        });

    </script>

@endsection