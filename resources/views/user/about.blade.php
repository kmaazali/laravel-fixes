@extends('layouts.user.master')

@section('content')
    @include('layouts.user.advertisement')

    <section id="sec-02">
        <div class="row">

                <div class="col-md-12">
                    <h4>About Us</h4>

                    <div class="col-md-12">
                        N-Blogger è una giovane realtà che vuole far incontrare lettori e scrittori di blog in un unico sito. Una persona intenzionata ad aprire un blog potrà farlo sul nostro sito e non dovrà pensare a nulla se non a scrivere gli articoli e condividerli. La cosa veramente straordinaria è che grazie agli annunci pubblicitari inseriti in automatico sulle pagine, il blogger guadagnerà proporzionalmente al numero delle pagine visionate. Il compito di ogni blogger è quello di scrivere gli articoli e condividerli il più possibile per aumentare fama e guadagni. Ogni lettore potrà leggere gli articoli che più gli piacciono, cercandoli nel finder del sito, oppure potrà seguire un blogger ed essere sempre aggiornato sulle sue ultime novità. In ogni articolo ci sono fantastiche funzionalità come il text to speech, utile per i non vedenti oppure per chi è impossibilitato in quel momento a leggere ma vuole essere sempre aggiornato. E' possibile inoltre salvare un articolo per potervi accedere più velocemente le volte successive. E' possibile accedere alla storie degli articoli letti oppure etichettare un articolo con leggi dopo. Questo sito unisce i lettori e i blogger di tutto il mondo, anche grazie all'utile funzionalità di traduzione. Non dovrai più perdere del tempo nel farti trovare, perchè saranno gli utenti stessi che verranno a trovarti. Inizia subito a scrivere ma sopratutto, inizia subito a guadagnare!
                    </div>
                </div>

        </div>


    </section>

@endsection
