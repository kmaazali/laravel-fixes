<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



Route::get('/', 'MainController@index')->name('index');
Route::get('/about-us','MainController@about')->name('about');
//
//Route::get('terms', function () {
//    return view('terms');
//});
//
//Route::get('policies', function () {
//    return view('policies');
//});


Route::get('image/upload','NewsController@images');
Route::get('test/{name}','CategoryController@index');
Route::get('/edit/profile', 'ProfileController@edit');
Route::post('/update/profile', 'ProfileController@update');

Route::get('/user/unsub/{id}','ProfileController@unsub');
Route::get('/user/sub/{id}','ProfileController@sub');


Route::get('profile', function () {
    return redirect('profile/' . Auth::user()->username);
});
Route::get('profile/{username}', 'ProfileController@index');
Route::get('timeline', function () {
    return redirect('timeline/' . Auth::user()->username);
});
Route::get('timeline/{username}', 'ProfileController@timeline');

Route::get('payments', 'ProfileController@payments');

Route::get('profile/{username}/presentation', 'ProfileController@presentation');

Route::get('notices', 'ProfileController@notices');

Route::get('follow/{username}', 'ProfileController@follow');
Route::get('unfollow/{username}', 'ProfileController@unfollow');


Route::get('category/{name}', 'CategoryController@index');


Route::get('readlater/{news_id}', 'MainController@read_later');
Route::get('myreads', 'MainController@my_reads');
Route::get('my_drafts', 'NewsController@my_drafts');


Route::resource('news', 'NewsController');
Route::get('togglebookmark/{id}', 'NewsController@toggle_bookmark');

Route::post('comment/publish/{id}', 'NewsController@publish_comment');
Route::get('comment/delete/{id}', 'NewsController@delete_comment');

Route::get('bookmarks', 'NewsController@get_bookmarks');
Route::get('history', 'NewsController@get_history');

Route::post('search-news', 'NewsController@search');
Route::get('delete/news/{id}','NewsController@delete');


Route::get('withdraw-paypal', 'AdminControllers\PaymentController@withdraw_paypal');
Route::get('withdraw-bank', 'AdminControllers\PaymentController@withdraw_bank');



Route::get('information', 'MainController@info_page');
Route::get('legal', 'MainController@legal_page');


Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function($router) {
    Route::get('/', 'AdminController@index');

    Route::resource('categories', 'AdminControllers\CategoryController');
    Route::resource('admins', 'AdminControllers\AdminResource');
    Route::resource('users', 'AdminControllers\UserResource');
    Route::resource('news', 'AdminControllers\NewsResource');

    Route::get('/user/{id}/suspend', 'AdminControllers\UserResource@suspend');
    Route::get('/user/{id}/unsuspend', 'AdminControllers\UserResource@unsuspend');

    Route::get('distributefunds', 'AdminControllers\PaymentController@distribute_view');
    Route::post('distributefunds', 'AdminControllers\PaymentController@distribute_funds');

    Route::get('payrequests', 'AdminControllers\PaymentController@payment_requests');
    Route::get('payrequests/paid/{id}', 'AdminControllers\PaymentController@mark_paid');

    Route::group(['prefix' => 'ads'], function($router) {
        Route::get('/', 'AdminController@ads');
        Route::post('store', 'AdminController@ads_store');
    });

    Route::group(['prefix' => 'settings'], function($router) {
        Route::get('/', 'AdminController@settings');
        Route::post('store', 'AdminController@settings_store');
    });

});