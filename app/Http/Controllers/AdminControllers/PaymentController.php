<?php

namespace App\Http\Controllers\AdminControllers;

use Auth;
use Session;

use App\Payment;
use App\PaymentDistribution;
use App\View;
use App\Withdrawal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function distribute_view() {
        return view('admin.payments.distribute-funds');
    }

    public function distribute_funds(Request $request) {
        $df = new PaymentDistribution;
        $df->amount = $request->amount;
        $df->save();

        $views = View::where('revenue_views', '!=' , 0)->get();

        $total_revenue_views = View::sum('revenue_views');

        if ($total_revenue_views == 0) {
            Session::flash('error', 'No Revenue Views, Payment Can not be distributed!');
            return back();
        }

        foreach ($views as $view) {
            $payment = Payment::where('user_id', $view->news->user->id)->where('paid', 0)->first();
            if (!$payment) {
                $payment = new Payment;
                $payment->user_id = $view->news->user->id;
                $payment->amount = 0;
            }

            $view_pay_amount = ($view->revenue_views / $total_revenue_views) * $request->amount;

            $payment->amount += $view_pay_amount;
            $payment->save();

            $v = View::find($view->id);
            $v->revenue_views = 0;
            $v->save();
        }

        Session::flash('success', 'Fund Distribution Added');
        return back();
    }

    private function withdraw($payment_id, $type) {
        $withdrawal = new Withdrawal;

        $withdrawal->user_id = Auth::user()->id;
        $withdrawal->payment_id = $payment_id;
        $withdrawal->method = $type;

        $withdrawal->save();
    }

    public function withdraw_paypal(Request $request) {
        if(Auth::guest()) {
            return redirect('/');
        }

        if($request->aa < $request->mw) {
            Session::flash('error', 'Available amount is less than minimum withdrawal limit');
            return back();
        }

        if(Auth::user()->paypal_email == "" || !Auth::user()->paypal_email) {
            Session::flash('info', 'Add Paypal email');
            return redirect('edit/profile')->with('pay_pal','paypal');
        }

        $type = 'paypal';

        $this->withdraw($request->pay_id, $type);

        Payment::where('user_id', Auth::user()->id)->where('paid', '0')->update(['paid' => 1]);

        Session::flash('success', 'Withdrawal Request initiated');

        return back();
    }

    public function withdraw_bank(Request $request) {
        if(Auth::guest()) {
            return redirect('/');
        }

        if($request->aa < $request->mw) {
            Session::flash('error', 'Available amount is less than minimum withdrawal limit');
            return back();
        }

        if(Auth::user()->bank_iban == "" || !Auth::user()->bank_iban || Auth::user()->bank_account_holder == "" || !Auth::user()->bank_account_holder) {
            Session::flash('error', 'Add Bank Details');
            return redirect('edit/profile')->with('pay_bank','bank');
        }

        $type = 'bank';

        $this->withdraw($request->pay_id, $type);

        Payment::where('user_id', Auth::user()->id)->where('paid', '0')->update(['paid' => 1]);

        Session::flash('success', 'Withdrawal Request initiated');

        return back();

    }

    public function payment_requests() {
        $withdrawals = Withdrawal::get();

        return view('admin.payments.payrequests', compact('withdrawals'));
    }

    public function mark_paid($id) {
        $withdrawal = Withdrawal::find($id);

        $payment = Payment::find($withdrawal->payment->id);
        $payment->paid = 2;
        $payment->save();

        $withdrawal->delete();


        Session::flash('success', 'Payment marked as Paid!');
        return back();
    }
}
