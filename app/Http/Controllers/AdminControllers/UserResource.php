<?php

namespace App\Http\Controllers\AdminControllers;

use Auth;
use Hash;
use Session;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            'password' => 'required|string|min:6|confirmed',
            'date_of_birth' => 'required',
        ]);

        $data = $request->all();

        try {
            $user = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => Hash::make($data['password']),
                'date_of_birth' => $data['date_of_birth'],
                'role_id' => $data['role'],
                'small_presentation' => '',
                'name_of_presentation' => '',
                'profile_picture' => '',
            ]);

            Session::flash('success', 'User created');
            return back();
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'date_of_birth' => 'required',
        ]);

        $user = User::find($id);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        $user->date_of_birth = $request->date_of_birth;
        $user->role_id = $request->role;
        $user->save();

        Session::flash('success', 'User Updated!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (Auth::user()->id == $id) {
                Session::flash('error', 'Can not remove self');
                return back();
            }

            $user = User::find($id);

            $user->delete();

            Session::flash('info', 'User Deleted');
            return back();

        } catch (Exception $e) {
            Session::flash('error', 'Failed to delete user');
            return back();
        }
    }

    public function suspend($id) {
        try {
            $user = User::find($id);

            if(Auth::user()->id == $user->id) {
                Session::flash('error', "You can't suspend yourself!");
                return back();
            }

            if ($user->suspended == 1) {
                Session::flash('info', 'User is already suspended');
                return back();
            }

            $user->role_id = 1;
            $user->suspended = 1;

            $user->save();

            Session::flash('info', 'User Suspended!');
            return back();

        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return back();
        }
    }

    public function unsuspend($id) {
        try {
            $user = User::find($id);

            if (Auth::user()->id == $user->id) {
                Session::flash('error', "You can't unsuspend yourself!");
                return back();
            }

            if ($user->suspended == 0) {
                Session::flash('info', 'User is not suspended');
                return back();
            }

            $user->suspended = 0;
            $user->save();

            Session::flash('info', 'User Activated!');
            return back();
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return back();
        }
    }
}
