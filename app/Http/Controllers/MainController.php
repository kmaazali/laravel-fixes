<?php

namespace App\Http\Controllers;

use App\ReadHistory;
use Auth;
use Session;

use App\Category;
use App\News;
use App\ReadLater;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request) {
        if (Auth::user()) {
            $recent_reads = ReadHistory::where('user_id', Auth::user()->id)->orderBy('created_at')->limit(5)->get();
        } else {
            $recent_reads = ReadHistory::where('ip', $request->ip)->orderBy('created_at')->limit(5)->get();
        }

        $news = array();

        foreach ($recent_reads as $rl) {
            $news[] = News::find($rl->news->id);
        }

        $recentCats = array();

        foreach ($news as $recent_new) {
            $recentCats[] = $recent_new->category->id;
        }

        $boi_news = News::whereIn('category_id', $recentCats)->where('draft', 0)->where('is_published',1)->whereHas('user', function ($query) {
            $query->where('suspended', '!=', '1');
        })->inRandomOrder()->get();


        $recent_news = News::where('draft', 0)->orderBy('created_at')->limit(2)->get();
        $categoriesArray = [];

        foreach ($recent_news as $recent_new) {
            $categoriesArray[] = $recent_new->category->id;
        }

        $feedcategories = Category::whereIn('id', $categoriesArray)->get();

        $news1 = "";
        $news2 = "";

        foreach ($feedcategories as $key => $category) {
            if ($key == 0) {
                $news1 = News::where('category_id', $category->id)->where('draft', 0)->where('is_published',1)->whereHas('user', function ($query) {
                    $query->where('suspended', '!=', '1');
                })->limit(6)->get();

            } else {
                $news2 = News::where('category_id', $category->id)->where('draft', 0)->where('is_published',1)->whereHas('user', function ($query) {
                    $query->where('suspended', '!=', '1');
                })->limit(6)->get();
            }
        }

        return view('user.index', compact('boi_news','feedcategories', 'news1', 'news2'));
    }

    public function about(){
        return view('user.about');
    }

    public function read_later($news_id) {
        try {
            if(Auth::guest()) {
                return redirect('/login');
            }
            $user = Auth::user();

            $exists = ReadLater::where('user_id', $user->id)->where('news_id', $news_id)->count();

            if ($exists > 0) {
                Session::flash('info', 'Already in Read Later');
                return back();
            }

            $rl = new ReadLater;

            $rl->user_id = $user->id;
            $rl->news_id = $news_id;

            $rl->save();

            Session::flash('success', 'Added to Read Later');
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
        }

        return back();
    }

    public function my_reads() {
        $rls = ReadLater::where('user_id', Auth::user()->id)->paginate(5);

        $news = array();

        foreach ($rls as $rl) {
            $news[] = News::find($rl->news->id);
        }

        return view('user.news.list', compact('rls', 'news'));
    }

    public function about_page() {
        return view('user.pages.about');
    }

    public function legal_page() {
        return view('user.pages.legal');
    }

    public function info_page() {
        return view('user.pages.information');
    }
}
