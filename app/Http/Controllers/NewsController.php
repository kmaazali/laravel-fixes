<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\Category;
use App\Collaboration;
use App\Follower;
use App\NewsComment;
use App\ReadHistory;

use App\ReadLater;

use App\User;
use App\View;
use Auth;
use Illuminate\Support\Facades\Input;
use Session;

use DB;
use Carbon\Carbon;

use App\News;

use Illuminate\Http\Request;



class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't add news");
            return back();
        }

        $categories = Category::orderBy('name')->get();
        $user = Auth::user();

        return view('user.news.create', compact('categories', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't add news");
            return back();
        }

        if($request->news_content == "" || is_null($request->news_content)) {
            Session::flash('error', 'News Content can not be empty');
            return back()->withInput();
        }

        try {
            $news = new News;
            $news->title = $request->title;
            $news->short_description = $request->short_description;
            $news->content = $request->news_content;

            $news->user_id = Auth::user()->id;
            $news->category_id = $request->category;

            if ($request->date_of_pub) {
                $news->schedule_time = $request->date_of_pub;
                //dd($request->date_of_pub);
                //dd(date("Y-m-d"));
                if($request->date_of_pub==date("Y-m-d")){
                  $news->is_published=1;
                }

            } else {
                $news->schedule_time = Carbon::now();
                $news->is_published=1;
            }

            $featuredImage = $request->file('file1');
            $featuredImageSaveAsName = time() . Auth::id() . "-post." .
                $featuredImage->getClientOriginalExtension();
            $ext=strtolower($featuredImage->getClientOriginalExtension());




            $upload_path = 'post_images/';
            $featured_image_url = $upload_path . $featuredImageSaveAsName;
            $success = $featuredImage->move($upload_path, $featuredImageSaveAsName);

            $news->featured_image = "/" . $featured_image_url;

            $news->save();

            $collabs = $request->collaborators;
            $collabs = explode(',', $collabs);

            Follower::where('user_id',Auth::user()->id)->increment('post_count');

            foreach ($collabs as $collab) {
                $collaborator = User::where('username', $collab)->first();
                if ($collaborator && $collaborator->suspended == 0) {
                    $c_id = $collaborator->id;

                    $c = new Collaboration;
                    $c->user_id = $c_id;
                    $c->news_id = $news->id;
                    $c->save();
                }

            }

            if($request->submit == 'draft') {
                $news->draft = 1;
                $news->save();
            } else {
                $request->session()->put('success', 'News Created Successfully!');
            }

            $views = new View;
            $views->news_id = $news->id;
            $views->revenue_views = 0;
            $views->views = 0;
            $views->save();
        } catch (Exception $e) {
            $request->session()->put('error', $e->getMessage());
        }

        return redirect('/timeline');

    }


    public function images(){
        // Include the editor SDK.

// Store the image.
try {
  $response = FroalaEditor_Image::upload('/uploads/');
  echo stripslashes(json_encode($response));
}
catch (Exception $e) {
  http_response_code(404);
}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        $news = News::find($id);
        if (!empty ($news)) {

            //if ($news->draft == 1 && Auth::user()->id != $news->user->id) {
            if ($news->draft == 1 || $news->is_published==0 && Auth::user()->id != $news->user->id)  {
                Session::flash('warning', 'You are not allowed to access this news');
            }

            if($news->is_published==0 && Auth::user()->id != $news->user->id){
                Session::flash('error', 'You are not allowed to access this news at this time');
                return back();
            }

            if ($news->user->suspended != 0 && Auth::user()->role_id != 2) {
                Session::flash('error', 'Author of this news is suspended, so this news is not available anymore');
                return back();
            }

            if (Auth::user()) {
                $bm = Bookmark::where('user_id', Auth::user()->id)->where('news_id', $id)->first();
            } else {
                $bm = false;
            }

            if ($bm) {
                $is_bookmarked = true;
            } else {
                $is_bookmarked = false;
            }

            $collaborators = Collaboration::where('news_id', $news->id)->get();

            $users = [];

            foreach ($collaborators as $collaborator) {
                $users[] = $collaborator->user_id;
            }

            $collaborators = User::whereIn('id', $users)->get();

            $popular_posts=News::orderBy('schedule_time','ASC')->paginate(3);
            $viewscount=View::select('news_id')->orderBy('views','ASC')->pluck('news_id');
            $hits=News::whereIn('id',$viewscount)->paginate(3);



            if (Auth::user()) {
                $exists = ReadHistory::where('user_id', Auth::user()->id)->where('news_id', $id)->count();

                if ($exists == 0) {
                    $rh = new ReadHistory;

                    $rh->user_id = Auth::user()->id;
                    $rh->news_id = $id;
                    $rh->ip = $request->ip();

                    $rh->save();

                    if (Auth::user()->role_id != 2) {
                        if (View::where('news_id', $id)->count() == 0) {
                            $views = new View;
                            $views->news_id = $news->id;
                            $views->revenue_views = 0;
                            $views->views = 0;
                            $views->save();
                        }

                        DB::table('views')->where('news_id', $id)->increment('views');
                        DB::table('views')->where('news_id', $id)->increment('revenue_views');
                    }
                }
            } else {
                $exists = ReadHistory::where('ip', $request->ip())->where('news_id', $id)->count();
                if ($exists == 0) {
                    $rh = new ReadHistory;

                    $rh->news_id = $id;
                    $rh->ip = $request->ip();

                    $rh->save();

                    if (Auth::user()) {
                        if (Auth::user()->role_id != 2) {
                            if (View::where('news_id', $id)->count() == 0) {
                                $views = new View;
                                $views->news_id = $news->id;
                                $views->revenue_views = 0;
                                $views->views = 0;
                                $views->save();
                            }

                            DB::table('views')->where('news_id', $id)->increment('views');
                            DB::table('views')->where('news_id', $id)->increment('revenue_views');
                        }
                    } else {
                        if (View::where('news_id', $id)->count() == 0) {
                            $views = new View;
                            $views->news_id = $news->id;
                            $views->revenue_views = 0;
                            $views->views = 0;
                            $views->save();
                        }

                        DB::table('views')->where('news_id', $id)->increment('views');
                        DB::table('views')->where('news_id', $id)->increment('revenue_views');
                    }
                }
            }

            return view('user.news.details', compact('news', 'collaborators', 'is_bookmarked','popular_posts','hits'));
        }
        else{
            return '<script type="text/javascript">alert("No Notices Found!");window.location.href="/news/create";</script>';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't edit news");
            return back();
        }

        $news = News::find($id);
        $user = Auth::user();
        $categories = Category::orderBy('name')->get();

        $collaborators = Collaboration::where('news_id', $news->id)->get();

        $users = [];

        foreach ($collaborators as $collaborator) {
            $users[] = $collaborator->user_id;
        }

        $collabs = User::whereIn('id', $users)->get();
        $collabsarray = $collabs->toArray();

        $collaborators = "";

        $keys = array_keys($collabsarray);

        $last_key = end($keys);
        if($collabs->count() > 0) {
            foreach($collabs as $key => $collaborator) {
                if ($key == $last_key) {
                    $collaborators .= $collaborator->username;
                } else {
                    $collaborators .= $collaborator->username . ",";
                }
            }
        }

        return view('user.news.edit', compact('news', 'user', 'categories', 'collaborators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't edit news");
            return back();
        }

        try {
            $news = News::find($id);
            $news->title = $request->title;
            $news->short_description = $request->short_description;
            $news->content = $request->news_content;

            $news->user_id = Auth::user()->id;
            $news->category_id = $request->category;

            if ($request->date_of_pub) {
                $news->schedule_time = $request->date_of_pub;
            } else {
                $news->schedule_time = Carbon\Carbon::now();
            }

            if ($request->has('file1')) {
                $featuredImage = $request->file('file1');
                $featuredImageSaveAsName = time() . Auth::id() . "-post." .
                    $featuredImage->getClientOriginalExtension();

                $upload_path = 'post_images/';
                $featured_image_url = $upload_path . $featuredImageSaveAsName;
                $success = $featuredImage->move($upload_path, $featuredImageSaveAsName);

                $news->featured_image = "/" . $featured_image_url;
            }

            $news->save();

            $collabs = $request->collaborators;
            $collabs = explode(',', $collabs);

            foreach ($collabs as $collab) {
                $collaborator = User::where('username', $collab)->first();
                if ($collaborator) {
                    $c_id = $collaborator->id;

                    if (Collaboration::where('user_id', $c_id)->where('news_id', $news->id)->count() == 0) {
                        $c = new Collaboration;
                        $c->user_id = $c_id;
                        $c->news_id = $news->id;
                        $c->save();
                    }
                }
            }

            if($request->submit == 'draft') {
                $news->draft = 1;
                $news->save();
            } else {
                $news->draft = 0;
                $news->save();
                $request->session()->put('success', 'News Created Successfully!');
            }

            $request->session()->put('success', 'News Updated Successfully!');
        } catch (Exception $e) {
            $request->session()->put('error', $e->getMessage());
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't delete news");
            return back();
        }

        try {
            $news = News::find($id);
            $news->delete();

            $request->session()->put('success', 'News Deleted Successfully!');
        } catch (Exception $e) {
            $request->session()->put('success', $e->getMessage());
        }

        return redirect('/timeline');
    }
    public function delete($id)
    {
        if (Auth::user()->suspended == 1) {
            Session::flash('error', "Your account is suspended, so you can't delete news");
            return back();
        }


            $news = News::find($id);
            $news->delete();




        return redirect('/timeline');
    }

    public function search(Request $request) {



        $term = $request->term;



       // $news = News::orWhere('title', 'LIKE', "%{$term}%")->where('is_published',1)->whereHas('user', function ($query) {
       //     $query->where('suspended', '!=', '1');
        //})->paginate(1);

        $news = News::where('title', 'LIKE', "%{$term}%")->where('is_published',1)->whereHas('user', function ($query) {
            $query->where('suspended', '!=', '1');
        })->get();
        //dd($news);

        //$news = News::where('title', 'LIKE', '%' . $term . '%')->get();

//        $user=User::orWhere('first_name','LIKE','%'.$term.'%')->get();
        $user = User::where('first_name', 'LIKE', "%{$term}%")->orWhere('last_name', 'LIKE', "%{$term}%")->get();


        //return view('user.news.search', compact(['news','user']));
        return view('user.news.search', compact(['news','user']));
    }

    public function toggle_bookmark(Request $request, $id){
        if(Auth::guest()) {
            return redirect('login');
        }

        $bm = Bookmark::where('user_id', Auth::user()->id)->where('news_id', $id)->first();

        if($bm) {
            $bm->delete();

            Session::flash('error', 'Bookmark Removed!');
        } else {
            $bm = new Bookmark;
            $bm->user_id = Auth::user()->id;
            $bm->news_id = $id;

            $bm->save();

            Session::flash('success', 'News Bookmarked!');
        }

        return back();
    }

    public function get_bookmarks() {
        if(Auth::guest()) {
            return redirect('login');
        }
        $bms = Bookmark::where('user_id', Auth::user()->id)->paginate(5);

        $news = array();

        foreach ($bms as $bm) {
            $n = News::find($bm->news_id);
            array_push($news, $n);
        }

        return view('user.news.list', compact('news', 'bms'));
    }

    public function get_history() {
        $bms = ReadHistory::where('user_id', Auth::user()->id)->paginate(5);

        $news = array();

        foreach ($bms as $bm) {
            $n = News::find($bm->news_id);
            array_push($news, $n);
        }

        return view('user.news.list', compact('news', 'bms'));
    }

    public function publish_comment(Request $request, $id) {
        if (!Auth::user()) {
            return redirect('login');
        }

        $comment = new NewsComment;

        $comment->news_id = $id;
        $comment->user_id = Auth::user()->id;
        $comment->content = $request->comment_content;
        $comment->notification = 0;

        $comment->save();

        Session::flash('success', 'Comment Posted!');
        return back();
    }

    public function delete_comment(Request $request, $id) {
        if (!Auth::user()) {
            return redirect('login');
        }

        $comment = NewsComment::find($id);

        $comment->delete();

        Session::flash('error', 'Comment Deleted!');
        return back();
    }

    public function my_drafts() {
        if (!Auth::user()) {
            return redirect('login');
        }

        $news = News::where('user_id', Auth::user()->id)->where('draft', 1)->get();

        return view('user.news.list', compact( 'news'));
    }
}
