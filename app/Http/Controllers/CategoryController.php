<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use Illuminate;
use App\Category;
use App\News;
use App\User;

class CategoryController extends Controller
{


    function custom_paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet    = ($pageStart * $perPage) - $perPage;
        // $itemsForCurrentPage = array_slice($items, $offSet, $perPage, TRUE);
        $itemsForCurrentPage = $items->slice($offSet, $perPage);
        return new Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }


    public function index($name) {

        $category = Category::where('name', $name)->first();

        $news = News::where('category_id', $category->id)->whereHas('user', function ($query) {
            $query->where('suspended', '!=', '1');
        })->paginate(5);
//        $news=$this->custom_paginate($news,6);


        return view('user.news.list', compact('news'));
       // dd($news);
    }
}
