<?php

namespace App\Http\Controllers;

use App\Setting;
use Session;

use App\Advertisment;
use App\News;
use App\ReadHistory;
use App\User;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {
        $news_count = News::whereHas('user', function ($query) {
            $query->where('suspended', '!=', '1');
        })->count();
        $users_count = User::where('role_id', 1)->where('suspended', 0)->count();
        $news_view_count = ReadHistory::count();

        return view('admin.index', compact('news_count', 'users_count', 'news_view_count'));
    }

    public function ads() {
        $ads = Advertisment::get();

        return view('admin.ads', compact('ads'));
    }

    public function ads_store(Request $request) {
        $ads = Advertisment::get();

        foreach ($ads as $ad) {
            $adv = Advertisment::find($ad->id);
            $name = "ad" . $ad->id;
            $adv->script = $request->$name;
            $adv->save();
        }

        Session::flash('success', 'Ads Updated!');
        return back();
    }

    public function settings() {
        $min_withdraw = Setting::where('key', 'min_withdraw')->first()->value;
        $currency_symbol = Setting::where('key', 'currency_symbol')->first()->value;

        return view('admin.settings', compact('min_withdraw', 'currency_symbol'));
    }

    public function settings_store(Request $request) {
        Setting::where('key', 'min_withdraw')->update(['value' => $request->min_withdraw]);
        Setting::where('key', 'currency_symbol')->update(['value' => $request->currency]);

        Session::flash('success', 'Settings Updated!');
        return back();
    }
}
