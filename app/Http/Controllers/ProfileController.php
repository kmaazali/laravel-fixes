<?php

namespace App\Http\Controllers;

use App\Follower;
use App\NewsComment;
use App\Payment;
use App\Setting;

use Illuminate\Http\Request;

use Auth;
use Session;

use App\User;
use App\News;

class ProfileController extends Controller
{
    public function index($username) {

        $user = User::where('username', $username)->first();



        return view('user.profile.view', compact('user'));
    }

    public function timeline($user) {

        $uid = User::where('username', $user)->first()->id;

        if(Auth::user()){
            $user_id=$uid;
            //dd($user_id);
            $logged_in_user_id=Auth::user()->id;

            //dd($logged_in_user_id);
            $follower=Follower::where('user_id',$user_id)->where('followed_id',$logged_in_user_id)->get();
            if(count($follower)>0) {
                Follower::where('user_id', $user_id)->where('followed_id', $logged_in_user_id)->update(['post_count' => 0]);


            }
        }

        $user = User::find($uid);
        $news = News::where('user_id', $uid)->where('draft', 0)->whereHas('user', function ($query) {
            $query->where('suspended', '!=', '1');
        })->paginate(5);

        $total_view_count = 0;
        foreach ($news as $new) {
            $total_view_count += $new->views->views;
        }

        return view('user.profile.timeline', compact('user','news', 'total_view_count'));
    }

    public function edit() {
        $user = Auth::user();

        return view('user.profile.edit-profile', compact('user'));
    }

    public function update(Request $request) {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            'password' => 'confirmed',
            'date_of_birth' => 'required',
        ]);

        try {
            $user = Auth::user();

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->date_of_birth = $request->date_of_birth;
            $user->name_of_presentation = $request->name_of_presentation;
            $user->small_presentation = $request->small_presentation;
            $user->paypal_email = $request->paypal_email;
            $user->bank_iban = $request->bank_iban;
            $user->bank_account_holder = $request->bank_account_name;

            if ($request->has('password')) {
                if($request->password != "") {
                    $user->password = bcrypt($request->password);
                }
            }

            $profile_image_url = "";

            if ($request->hasFile('profile_picture')){
                $profileImage = $request->file('profile_picture');
                $profileImageSaveAsName = time() . Auth::id() . "-profile." .
                    $profileImage->getClientOriginalExtension();

                $upload_path = 'profile_images/';
                $profile_image_url = $upload_path . $profileImageSaveAsName;
                $success = $profileImage->move($upload_path, $profileImageSaveAsName);
            }

            $user->profile_picture = $profile_image_url;

            $user->save();

            Session::flash('success', 'Profile Updated Successfully!');
            return back();

        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return back();
        }
    }

    public function presentation($username) {
        $user = User::where('username', $username)->first();

        $followers = Follower::where('followed_id', $user->id)->paginate(8);

        $is_follower = null;
        if(Auth::user())
            $is_follower = Follower::where('user_id', Auth::user()->id)->where('followed_id', $user->id)->count();

        return view('user.profile.presentation', compact('user','followers', 'is_follower'));
    }

    public function follow($username) {
        try {
            $user = User::where('username', $username)->first();

            $count = Follower::where('user_id', Auth::user()->id)->where('followed_id', $user->id)->count();

            if($count == 0) {
                $follower = new Follower;

                $follower->user_id = Auth::user()->id;
                $follower->followed_id = $user->id;

                $follower->save();

                Session::flash('success', 'You are now following ' .  $username);
            } else {
                Session::flash('info', 'You are already following this person');
            }

            return back();
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
        }
    }

    public function unfollow($username) {
        try {
            $user = User::where('username', $username)->first();

            $count = Follower::where('user_id', Auth::user()->id)->where('followed_id', $user->id)->count();

            if($count != 0) {
                Follower::where('user_id', Auth::user()->id)->where('followed_id', $user->id)->delete();

                Session::flash('success', 'Unfollowed ' .  $username);
            } else {
                Session::flash('info', 'You are not a follower of this person');
            }

            return back();
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
        }
    }

    public function payments() {
        if(Auth::user()) {
            $amount_withdrawn = Payment::where('user_id', Auth::user()->id)->where('paid', 2)->sum('amount');

            $amount_pending = Payment::where('user_id', Auth::user()->id)->where('paid', 1)->sum('amount');

            $amount_available = Payment::where('user_id', Auth::user()->id)->where('paid', 0)->sum('amount');

            $min_withdraw = Setting::where('key', 'min_withdraw')->first()->value;
            $currency_symbol = Setting::where('key', 'currency_symbol')->first()->value;

            $payment = Payment::where('user_id', Auth::user()->id)->where('paid', 0)->first();
            $user = Auth::user();

            return view('user.profile.payment', compact('amount_withdrawn', 'amount_pending', 'amount_available', 'user', 'min_withdraw', 'currency_symbol', 'payment'));
        } else {
            return redirect('/login');
        }
    }

    public function notices() {
        if (Auth::user()) {
            $user = Auth::user();

            $news = News::where('user_id', $user->id)->get();

            $newsArray = array();

            foreach ($news as $new) {
                $newsArray[] = $new->id;
            }

            $notices = NewsComment::where('notification', 0)->whereIn('news_id', $newsArray)->orderBy('created_at', 'DESC')->paginate(5);
            NewsComment::whereIn('news_id', $newsArray)->update(['notification' => 1]);

            return view('user.profile.notices', compact('user', 'notices'));
        }
    }

    public function unsub($id){

        $user=User::where('id',$id)->update(['suspended'=>1]);
        $user=User::where('id',$id)->first();
        return view('user.profile.view', compact('user'));

    }
    public function sub($id){
        $user=User::where('id',$id)->update(['suspended'=>0]);
        $user=User::where('id',$id)->first();
        return view('user.profile.view', compact('user'));

    }
}
