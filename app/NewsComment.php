<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsComment extends Model
{
    protected $fillable = [
        'news_id',
        'user_id',
        'content',
        'notification'
    ];

    public function news() {
        return $this->belongsTo('App\News');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
