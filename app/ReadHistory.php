<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadHistory extends Model
{
    protected $fillable = [
        'user_id',
        'news_id',
        'ip'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function news() {
        return $this->belongsTo('App\News');
    }
}
