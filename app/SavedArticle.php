<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedArticle extends Model
{
    protected $fillable = [
        'table_id',
        'save_id'
    ];
}
