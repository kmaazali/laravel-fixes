<?php
    if (! function_exists('trans_fb')) {
        /**
         * Translate the given message with a fallback string if none exists.
         *
         * @param  string  $id
         * @param  string  $fallback
         * @param  array   $parameters
         * @param  string  $domain
         * @param  string  $locale
         * @return \Symfony\Component\Translation\TranslatorInterface|string
         */
        function trans_fb($id, $fallback, $parameters = [], $domain = 'content', $locale = null)
        {
            return ($id === ($translation = trans($id, $parameters, $domain, $locale))) ? $fallback : $translation;
        }
    }