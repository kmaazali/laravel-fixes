<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = [
        'news_id',
        'revenue_views',
        'views'
    ];

    public function news() {
        return $this->belongsTo('App\News');
    }
}
