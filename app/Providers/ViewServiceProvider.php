<?php

namespace App\Providers;

use App\Follower;
use Illuminate\Support\ServiceProvider;

use Auth;

use App\User;
use App\Category;
use App\News;
use App\NewsComment;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view)
        {
            $categories = Category::where('parent_id', 0)->get();
            $subcategories = Category::where('parent_id', '!=', '0')->get();
            if (Auth::user()) {
                $follwees = Follower::where('user_id', Auth::user()->id)->get();
                $follows = [];
                foreach ($follwees as $follwee) {
                    $follows[] = $follwee->followed_id;
                }

                $bloggers = User::whereIn('id', $follows)->where('id', '!=', Auth::user()->id)->with('follower')->inRandomOrder()->limit(4)->get();
                //dd($bloggers);
            } else {
                $bloggers = User::where('role_id', 1)->inRandomOrder()->limit(4)->get();
            }

            $notice_count = 0;

            if (Auth::user()) {
                $news = News::where('user_id', Auth::user()->id)->get();

                $newsArray = array();

                foreach ($news as $new) {
                    $newsArray[] = $new->id;
                }

                $notice_count = NewsComment::where('notification', 0)->whereIn('news_id', $newsArray)->orderBy('created_at', 'DESC')->count();
            }

            $view->with('categories', $categories)->with('subcategories', $subcategories)->with('bloggers', $bloggers)->with('notice_count', $notice_count);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
