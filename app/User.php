<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'password',
        'date_of_birth',
        'name_of_presentation',
        'profile_picture',
        'small_presentation',
        'paypal_email',
        'bank_iban',
        'bank_account_holder',
        'role_id',
        'suspended',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function news() {
        return $this->hasMany('App\News');
    }

    public function readlater() {
        return $this->hasMany('App\ReadLater');
    }

    public function readhistory() {
        return $this->hasMany('App\ReadHistory');
    }

    public function follower() {
        return $this->hasMany('App\Follower');
    }

    public function bookmark() {
        return $this->hasMany('App\Bookmark');
    }

    public function singlefollow(){
        return $this->belongsTo('App\Follower');
    }
}
