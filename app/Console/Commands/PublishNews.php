<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;

class PublishNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PublishNews:publishthenews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish News Scheduled';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        DB::table('news')->whereDate('schedule_time', '>=', Carbon::today()->toDateString())->update(['is_published' => 1]);
    }
}
