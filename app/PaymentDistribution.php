<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDistribution extends Model
{
    protected $fillable = [
        'amount',
    ];
}
