<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'short_description',
        'content',
        'featured_image',
        'schedule_time',
        'user_id',
        'category_id',
        'draft',
        'is_published'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function readlater() {
        return $this->belongsToMany('App\ReadLater');
    }

    public function readhistory() {
        return $this->belongsToMany('App\ReadHistory');
    }

    public function views() {
        return $this->hasOne('App\View');
    }

    public function comments() {
        return $this->hasMany('App\NewsComment');
    }
}
