<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    protected $fillable = [
        'user_id',
        'payment_id',
        'method'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function payment() {
        return $this->belongsTo('App\Payment');
    }
}
