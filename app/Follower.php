<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $fillable = [
        'user_id',
        'followed_id',
        'post_count'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
