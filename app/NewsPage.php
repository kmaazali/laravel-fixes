<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsPage extends Model
{
    protected $fillable = [
        'news_id',
        'title',
        'position',
        'indent'
    ];
}
